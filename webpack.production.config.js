const config = require('./webpack.config');
const webpack = require('webpack');

module.exports = {
	...config,
	mode: 'production',
	// performance: {
	// 	hints: 'warning',
	// 	maxEntrypointSize: 300 * 1000, // цифру можно менять в зависимости от размера приложения
	// },
	devtool: false,
};
