declare module "*.svg" {
	const content: any;
	export default content;
}

// declare module '*.scss';

declare module '*.scss' {
	interface IClasses {
		[className: string]: string;
	}
	const classes: IClasses;
	export = classes;
}

declare module '*.css' {
	interface IClasses {
		[className: string]: string;
	}
	const classes: IClasses;
	export = classes;
}

declare module "*.png" {
	const value: any;
	export default value;
}