import { action, makeAutoObservable, observable } from 'mobx';
import { api } from '../api';
import { IPoint } from '../interfaces/IPoint';
import { IAction } from '../interfaces/IAction';

class AdminStore {
	@observable fullPoints: IPoint[] = null;
	@observable actions: IAction[] = [];

	constructor() {
		makeAutoObservable(this);
	}

	@action denyRequests = async (ids: number[] = []) => {
		const promises: Promise<any>[] = [];
		ids.forEach((id) => {
			promises.push(api.denyRequest(id));
		});

		return await Promise.all(promises);
	};

	@action approveRequests = async (ids: number[] = []) => {
		const promises: Promise<any>[] = [];
		ids.forEach((id) => {
			promises.push(api.approveRequest(id));
		});

		return await Promise.all(promises);
	};

	@action unpublishPPoints = async (ids: number[]) => {
		return await api.unpublishPoints(ids);
	};

	@action publishPoints = async (ids: number[]) => {
		return await api.publishPoints(ids);
	};

	@action getFullPoints = async () => {
		try {
			this.fullPoints = await api.getFullPoints();
		} catch (e) {}
	};

	@action getActions = async () => {
		try {
			const response = await api.getActions({
				desc: true,
			});
			this.actions = response?.actions;

			return this.actions;
		} catch (e) {
			return Promise.reject(e);
		}
	};
}

export default new AdminStore();
