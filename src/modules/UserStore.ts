import { action, computed, makeAutoObservable, observable } from 'mobx';
import { api } from '../api';
import { ILoginData, ISignupData, IUser, UserTypes } from '../interfaces/IUser';
import { ICoordinates, IPoint } from '../interfaces/IPoint';
import { IComplaint } from '../interfaces/IComplaint';
import { IComment } from '../interfaces/IComment';
// @ts-ignore
// import makeInspectable from 'mobx-devtools-mst';

export enum RequestStatus {
	ON_CHECK = 'ON_CHECK',
	APPROVED = 'APPROVED',
	DENIED = 'DENIED',
}

export interface Request {
	id: number;
	title: string;
	description: string;
	coordinates: ICoordinates;
	address: string;
	city: number;
	fractions: number[];
	status: RequestStatus;
	available: false;
	createdBy: number;
	updatedBy: number;
}

class UserStore {
	@observable user: IUser = null;
	@observable isLoading = false;
	@observable currentUserLoading = false;
	@observable initialLoaded = false;
	@observable error: string = null;
	@observable myRequests: Request[] = [];
	@observable allRequests: Request[] = [];
	@observable favoritePoints: IPoint[] = [];
	@observable myComplaints: IComplaint[] = [];
	@observable authModalVisible: boolean = false;
	@observable myComments: IComment[] = [];

	constructor() {
		makeAutoObservable(this);
	}

	@action setAuthModalVisible = (visible: boolean) => {
		this.authModalVisible = visible;
	};

	@action setUser = (user: IUser) => {
		this.user = user;
	};

	@action updateUser = (data: IUser) => {
		this.user = { ...this.user, ...data };
	};

	@action clearUser = () => {
		this.user = null;
	};

	@action clearError = () => {
		this.error = null;
	};

	@action login = async (credentials: ILoginData) => {
		this.isLoading = true;
		this.clearError();
		try {
			await api.login(credentials);
			return this.getCurrentUser();
		} catch (err) {
			console.log(err.response);
			if (err.response) {
				switch (err.response.status) {
					case 400:
						this.error = err.response.data?.message;
						break;
				}
			}
			return Promise.reject(this.error);
		}

		this.isLoading = false;
	};

	@action logout = async () => {
		try {
			await api.logout();
			this.clearUser();
			this.setAuthModalVisible(false);
		} catch (e) {
			return Promise.reject();
		}
	};

	@action signup = async (data: ISignupData) => {
		this.isLoading = true;

		try {
			await api.signup(data);
			return this.getCurrentUser();
		} catch (err) {
			if (!err.response) {
				return;
			}

			switch (err.response.status) {
				case 400:
					this.error = err.response.data.message;
					break;
			}

			return Promise.reject(this.error);
		}

		this.isLoading = false;
	};

	@computed get isLoggedIn(): boolean {
		return Boolean(this.user);
	}

	@computed get isModerator(): boolean {
		return this.user?.type === UserTypes.moderator;
	}

	@computed get isAdmin(): boolean {
		return this.user?.type === UserTypes.admin;
	}

	@computed get canSeeDashboard(): boolean {
		return this.isLoggedIn && (this.isAdmin || this.isModerator);
	}

	@action getCurrentUser = async (initial?: boolean) => {
		this.currentUserLoading = true;
		this.error = null;

		try {
			this.user = await api.getCurrentUser();
		} catch (error) {}

		this.currentUserLoading = false;

		if (initial) {
			this.initialLoaded = true;
		}
	};

	@action getMyRequests = async () => {
		this.myRequests = await api.getMyRequests();
		return this.myRequests;
	};

	@action getAllRequests = async () => {
		this.allRequests = await api.getRequests();
	};

	@action getFavoritePoints = async () => {
		this.favoritePoints = await api.getFavorites();
	};

	@action addToFavorites = async (id: number) => {
		await api.addToFavorites([id]);
		await this.getFavoritePoints();
	};

	@action removeFromFavorites = async (id: number) => {
		await api.removeFromFavorites([id]);
		await this.getFavoritePoints();
	};

	@action addComplaint = async (pointId: number, data: IComplaint) => {
		await api.addComplaint(pointId, data);
	};

	@action getMyComplaints = async () => {
		this.myComplaints = await api.getMyComplaints(this.user.login);
	};

	@action getComments = async () => {
		try {
			const comments = await api.getMyComments();
			this.myComments = comments?.reverse();
			return this.myComments;
		} catch (e) {
			return Promise.reject(e);
		}
	};
}

// const UserStoreContext: React.Context<UserStore> = React.createContext<UserStore>(null);
// export const UserStoreProvider = ({ children, store }: {store: UserStore, children: ReactChildren}) => {

// return (
// 	<UserStoreContext.Provider value={store}>{children}</UserStoreContext.Provider>
// );
// };
/* Hook to use store in any functional component */
// export const useStore = ()e => React.useContext(UserStoreContext);

// export default makeInspectable(new UserStore());
export default new UserStore();
