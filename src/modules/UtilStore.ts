import { action, makeAutoObservable, observable } from 'mobx';
import { api } from '../api';
import { citiesArr } from '../constants/cities';

export interface ICity {
	id: number;
	name: string;
}

const defaultCities = citiesArr;

class UtilStore {
	@observable cities: ICity[] = defaultCities;
	@observable sidebarOpen: boolean = true;
	// @observable addressInput: string = '';
	@observable dadataInput: any = null;

	constructor() {
		makeAutoObservable(this);
	}

	@action loadCities = async () => {
		try {
			this.cities = ((await api.loadCities()) as any).cities;
		} catch (e) {}
	};

	@action setSidebarOpen = (open: boolean) => {
		this.sidebarOpen = open;
	};

	// @action setAddressInput = (address: string) => {
	// 	this.addressInput = address;
	// };

	@action dadataSuggest = async (address: string) => {
		// return await api.geoCode(address);
		return await api.dadataSuggest(address);
	};

	@action mapboxSuggestions = async (address: string) => {
		return await api.mapboxSuggestions(address);
	};

	@action mapboxReverseGeocode = async (lng: number, ltd: number) => {
		return await api.mapboxReverseGeocode(lng, ltd);
	};

	@action setDadataInput = (data: any) => {
		this.dadataInput = data;
	};
}

export default new UtilStore();
