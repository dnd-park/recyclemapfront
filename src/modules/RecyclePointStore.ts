import { action, makeAutoObservable, observable } from 'mobx';
import { IPoint } from '../interfaces/IPoint';
import { api } from '../api';
import { FeatureCollection } from 'geojson';
import toGeoJson from '../utils/toGeoJson';
import { IFraction } from '../interfaces/IFraction';
import { fractionsDefault, mvtSourceUrl } from '../constants/constants';
import { mockPoints } from '../mocks/points';
import { IGrade } from '../interfaces/IGrade';
import { IComment } from '../interfaces/IComment';

class RecyclePointStore {
	@observable selectedPoint: IPoint = null;
	@observable points: IPoint[] = null;
	@observable pointsLoading: boolean = false;
	@observable geoJSONPoints: FeatureCollection = null;
	@observable fractions: IFraction[] = fractionsDefault;
	@observable createError: string = null;
	@observable mvtSourceTiles: string[] = [mvtSourceUrl];
	@observable selectedPointComments: IComment[] = [];

	constructor() {
		makeAutoObservable(this);
	}

	@action selectPoint = (point: IPoint) => {
		this.selectedPoint = point;
	};

	@action clearSelectedPoint = () => {
		this.selectedPoint = null;
	};

	@action insertPoint = async (point: IPoint) => {
		this.createError = null;
		try {
			return await api.insertPoint(point);
		} catch (e) {
			this.createError = 'Произошла ошибка. Повторите позже';
			return Promise.reject(this.createError);
		}
	};

	@action updatePoint = () => {};

	@action getAll = async () => {
		this.pointsLoading = true;

		this.points = await api.getPoints();
		// this.points = mockPoints;

		this.geoJSONPoints = toGeoJson(this.points);
		this.pointsLoading = false;
	};

	@action getPoint = async (id: number) => {
		try {
			this.selectedPoint = await api.getPoint(id);

			return this.selectedPoint;
		} catch (e) {
			return Promise.reject(e);
		}
	};

	@action toggleFraction = (fractionId: number) => {
		this.fractions.map((el) => {
			if (el.id === fractionId) {
				el.selected = !el.selected;
			}

			return el;
		});

		const filterFractionIds = this.fractions
			.filter((f) => f.selected)
			.map((el) => {
				return el.id;
			});

		// const filteredPoints = this.points.filter((point) => {
		// 	// пункт содержит все фракции, указанные в фильре
		// 	const hasFractions = !filterFractionIds.some((f: number) => {
		// 		return !point.fractions.includes(f);
		// 	});
		//
		// 	return hasFractions;
		// });

		// this.geoJSONPoints = toGeoJson(filteredPoints);

		this.mvtSourceTiles = [`${mvtSourceUrl}?fractions=${filterFractionIds.join(',')}&dt=${Date.now()}`];

		console.log(this.mvtSourceTiles);
	};

	@action uploadPhotos = async (pointId: number, files: FormData) => {
		try {
			await api.uploadPointPhotos(pointId, files);
		} catch (e) {}
	};

	@action uploadRequestPhotos = async (reqId: number, files: FormData) => {
		try {
			await api.uploadRequestPhotos(reqId, files);
		} catch (e) {}
	};

	@action insertRequest = async (point: IPoint) => {
		this.createError = null;
		try {
			return await api.insertRequest(point);
		} catch (e) {
			this.createError = 'Произошла ошибка. Повторите позже';
			return Promise.reject(this.createError);
		}
	};

	@action setGradeToPoint = async (grade: IGrade) => {
		try {
			await api.setGradeToPoint({ grades: [grade] });
			await this.getPoint(this.selectedPoint.id);
			return;
		} catch (e) {
			return Promise.reject(e);
		}
	};

	@action addComment = async (comment: IComment) => {
		try {
			await api.addComment(this.selectedPoint.id, comment);
			return await this.getComments(this.selectedPoint.id);
		} catch (e) {
			return Promise.reject(e);
		}
	};

	@action getComments = async (pointId: number) => {
		try {
			const comments = await api.getPointComments(pointId);
			this.selectedPointComments = comments?.reverse();
			return this.selectedPointComments;
		} catch (e) {
			return Promise.reject(e);
		}
	};
}

export default new RecyclePointStore();
