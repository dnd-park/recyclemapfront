import { action, makeAutoObservable, observable } from 'mobx';
import { MapboxGeoJSONFeature } from 'mapbox-gl';
import { ICoordinates } from '../interfaces/IPoint';
import { IMarker, IMarkers } from '../interfaces/IMarker';
import { createDonut } from '../utils/createDonut';
import { fractionsDefault } from '../constants/constants';

export enum MapMods {
	showPoints,
	addPoint,
}

class MapStore {
	@observable mode: MapMods = MapMods.showPoints;
	@observable viewport: any = {
		latitude: 55.749489,
		longitude: 37.619649,
		zoom: 10,
	};
	@observable layerPoints: any = {
		id: 'point',
		type: 'symbol',
		layout: {
			// 'icon-image': 'markerIcon',
			'icon-image': ['case', ['get', 'paper'], 'markerIcon', ['get', 'metal'], 'recyclingIcon', 'markerIcon'],
			'icon-size': 0.25,
			'icon-allow-overlap': true,
		},
		paint: {
			'icon-color': '#007cbf',
			'icon-halo-color': '#007cbf',
			'text-color': '#007cbf',
		},
		// filter: ['!', ['has', 'point_count']],
		filter: ['all', ['!', ['has', 'point_count']]],
	};

	@observable layerClusterCircle = {
		id: 'point-cluster-circle',
		type: 'circle',
		paint: {
			// 'circle-color': ['step', ['get', 'point_count'], '#68a558', 100, '#68a558', 750, '#68a558'],
			'circle-color': '#68a558',
			'circle-radius': ['step', ['get', 'point_count'], 15, 100, 20, 750, 25],
			// 'circle-radius': 15,
		},
		// filter: ['has', 'point_count'],
		filter: ['all', ['has', 'point_count']],
	};

	@observable layerClusterCount = {
		id: 'point-cluster-count',
		type: 'symbol',
		layout: {
			'text-field': '{point_count_abbreviated}',
			'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
			'text-size': 12,
		},
		paint: {},
		// filter: ['has', 'point_count'],
		filter: ['all', ['has', 'point_count']],
	};

	@observable layerPointCircle = {
		id: 'point-circle',
		type: 'circle',
		paint: {
			// 'circle-color': ['step', ['get', 'point_count'], '#68a558', 100, '#68a558', 750, '#68a558'],
			'circle-color': '#d3e7bf',
			// 'circle-radius': ['step', ['get', 'point_count'], 15, 100, 20, 750, 25],
			'circle-radius': 20,
			'circle-stroke-width': 1,
			'circle-stroke-color': '#fff',
		},
		// filter: ['!', ['has', 'point_count']],
		filter: ['all', ['!', ['has', 'point_count']]],
	};

	@observable newPointLngLat: number[] = null;
	@observable markersCache: IMarkers = {};
	@observable markersOnScreen: IMarker[] = [];

	constructor() {
		makeAutoObservable(this);
	}

	@action setMode = (mode: MapMods) => {
		this.mode = mode;
	};

	@action setViewport = (newViewport: any) => {
		this.viewport = newViewport;
	};

	@action setNewPointLngLat = (lngLat: number[]) => {
		this.newPointLngLat = lngLat;
	};

	@action updateMarkers = (features: MapboxGeoJSONFeature[]) => {
		const newMarkers: IMarkers = {};

		features.forEach((feature) => {
			const props = feature.properties;
			// работаем только с пунктами, кластеры не трогаем
			if (props.count > 1) {
				return;
			}

			const coordinates = (feature.geometry as ICoordinates).coordinates;
			const id: number = props.id;

			// берем из кэша
			let marker = this.markersCache[id];
			// если нет в кэше, то создаем новый маркер
			if (!marker) {
				const el = createDonut(props);

				props.fractionsArray = props.fractions.split(',').map((id: string) => {
					return fractionsDefault.find((f) => f.id === Number(id));
				});

				marker = this.markersCache[id] = {
					id,
					el,
					longitude: coordinates[0],
					latitude: coordinates[1],
					properties: props,
				};
			}

			newMarkers[id] = marker;
		});

		this.markersOnScreen = Object.keys(newMarkers).map((key) => {
			return newMarkers[Number(key)];
		});

		console.log('markersOnScreen length', this.markersOnScreen.length);
	};
}

export default new MapStore();
