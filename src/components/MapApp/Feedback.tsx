import { Button, Form, Input, Modal, Tooltip } from 'antd';
import Text from 'antd/es/typography/Text';
import React, { useState } from 'react';
import styles from './MapApp.scss';
import { ShareAltOutlined, MessageOutlined } from '@ant-design/icons';

export const Feedback = () => {
	const [visible, setVisible] = useState<boolean>();
	const [form] = Form.useForm();

	const onFinish = (values: any) => {
		console.log('Success:', values);

		setVisible(false);
	};

	const onFinishFailed = (errorInfo: any) => {
		console.log('Failed:', errorInfo);
	};

	return (
		<>
			<Tooltip title="Обратная связь" placement="left">
				<Button
					shape="circle"
					icon={<MessageOutlined />}
					className={styles.rightControl}
					onClick={() => setVisible(true)}
				/>
			</Tooltip>
			<Modal centered visible={visible} onCancel={() => setVisible(false)} footer={null} title="Обратная связь">
				<Text type="secondary">
					Расскажите что нужно менять на сайте, что нужно добавить или с какой проблемой Вы столкнулись
				</Text>

				<Form
					layout="vertical"
					name="basic"
					initialValues={{ remember: true }}
					onFinish={onFinish}
					onFinishFailed={onFinishFailed}
					requiredMark={false}
					form={form}
					style={{ marginTop: '20px' }}
				>
					<Form.Item
						// label="Сообщение"
						name="description"
						rules={[{ required: true, message: 'Поле "Сообщение" должно быть заполнено' }]}
					>
						<Input.TextArea rows={8} placeholder="Сообщение" />
					</Form.Item>

					<Form.Item>
						<Button type="primary" htmlType="submit">
							Отправить
						</Button>
					</Form.Item>
				</Form>
			</Modal>
		</>
	);
};
