import React from 'react';
import { observer } from 'mobx-react';
import SideBar from '../SideBar/SideBar';
import mapAppStyles from './MapApp.scss';
import HeaderControls from '../HeaderControls/HeaderControls';
import { ShareAltOutlined, MessageOutlined } from '@ant-design/icons';
import { Button, Tooltip } from 'antd';
import styles from './MapApp.scss';
import MvtMap from '../Map/MvtMap';
import { Feedback } from './Feedback';

const MapApp = observer(() => {
	return (
		<div className={mapAppStyles.MapApp}>
			<MvtMap />
			<div className={styles.rightControls}>
				{/*<Tooltip title="Поделиться" placement="left">*/}
				{/*	<Button shape="circle" icon={<ShareAltOutlined />} className={styles.rightControl} />*/}
				{/*</Tooltip>*/}
				<Feedback />
			</div>
			<HeaderControls />
			<SideBar />
		</div>
	);
});

export default MapApp;
