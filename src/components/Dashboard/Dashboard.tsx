import React from 'react';
import { observer } from 'mobx-react';
import styles from './Dashboard.scss';
import { EnvironmentOutlined, FileDoneOutlined } from '@ant-design/icons';
import { Route, Switch, useHistory, useLocation } from 'react-router-dom';

const Dashboard = observer(() => {
	return (
		<div style={{ height: '100%' }}>
			<SiderDemo />
		</div>
	);
});

export default Dashboard;

import { Button, Layout, Menu } from 'antd';
import {
	MenuUnfoldOutlined,
	MenuFoldOutlined,
	UserOutlined,
	VideoCameraOutlined,
	UploadOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import PointPanel from '../SideBar/PointPanel/PointPanel';
import UserPanel from '../SideBar/UserPanel/UserPanel';
import PointForm from '../SideBar/PointForm/PointForm';
import FractionPanel from '../SideBar/FractionPanel/FractionPanel';
import ManagePoints from './ManangePoints/ManagePoints';
import ManageRequests from './ManageRequests/ManageRequets';
import ManageUsers from './ManageUsers/ManageUsers';
import Actions from './Actions';

const { Header, Sider, Content } = Layout;

class SiderDemo extends React.Component {
	state = {
		collapsed: false,
	};

	toggle = () => {
		this.setState({
			collapsed: !this.state.collapsed,
		});
	};

	render() {
		return (
			<Layout
				style={{
					minHeight: '100%',
				}}
			>
				<Sider trigger={null} collapsible collapsed={this.state.collapsed}>
					<div className={styles.logo} />
					<Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
						<Menu.Item key="1" icon={<EnvironmentOutlined />}>
							<Link to="/dashboard/points">Пункты</Link>
						</Menu.Item>
						<Menu.Item key="2" icon={<FileDoneOutlined />}>
							<Link to="/dashboard/requests">Заявки</Link>
						</Menu.Item>
						{/*<Menu.Item key="3" icon={<FileDoneOutlined />}>*/}
						{/*	<Link to="/dashboard/Пользователи">Пользователи</Link>*/}
						{/*</Menu.Item>*/}
						<Menu.Item key="4" icon={<FileDoneOutlined />}>
							<Link to="/dashboard/actions">Действия</Link>
						</Menu.Item>
					</Menu>
				</Sider>
				<Layout className="site-layout">
					<Header className={styles.siteLayoutBackground} style={{ padding: 0 }}>
						{React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
							className: styles.trigger,
							onClick: this.toggle,
						})}
						<Link to="/" className={styles.toMapButton}>
							<Button>Перейти на карту</Button>
						</Link>
					</Header>
					<Content
						className={styles.siteLayoutBackground}
						style={{
							margin: '24px 16px',
							padding: 24,
							minHeight: 280,
						}}
					>
						<Switch>
							<Route path="/dashboard/points" component={ManagePoints} />
							<Route path="/dashboard/requests" component={ManageRequests} />
							<Route path="/dashboard/users" component={ManageUsers} />
							<Route path="/dashboard/actions" component={Actions} />
							<Route path="/" component={ManagePoints} />
						</Switch>
					</Content>
				</Layout>
			</Layout>
		);
	}
}
