import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import AdminStore from '../../modules/AdminStore';
import { List, Spin } from 'antd';
import moment from 'moment';
import { ActionItemTypes, ActionTypes, IAction } from '../../interfaces/IAction';

const itemTypes = {
	[ActionItemTypes.point]: 'пункт',
	[ActionItemTypes.request]: 'заявку',
};

const parseAction = (action: IAction) => {
	let act;
	switch (true) {
		case action.action === ActionTypes.denied:
			act = `отклонил заявку #${action.item.id} `;
			break;
		case action.action === ActionTypes.approved:
			act = `принял заявку #${action.item.id} `;
			break;
		case action.action === ActionTypes.created:
			act = `создал ${itemTypes[action.item.type]} <a href="/point/${action.item.id}" target="_blank">#${
				action.item.id
			} ${action.item.name}</a>`;
			break;
		case action.action === ActionTypes.published:
			act = `опубликовал пункт <a href="/point/${action.item.id}" target="_blank">#${action.item.id} ${action.item.name}</a>`;
			break;
		case action.action === ActionTypes.unpublished:
			act = `снял с публикации пункт <a href="/point/${action.item.id}" target="_blank">#${action.item.id} ${action.item.name}</a>`;
			break;
	}

	const result = `Пользователь #${action.actor.id} ${action.actor.name} ${act}`;

	return <span dangerouslySetInnerHTML={{ __html: result }} />;
};

const Actions = observer(() => {
	const { actions, getActions } = AdminStore;

	useEffect(() => {
		getActions();
	}, []);

	return (
		<div>
			{/*{JSON.stringify(actions, null, 4)}*/}
			<List
				dataSource={actions}
				itemLayout="vertical"
				renderItem={(item) => (
					<List.Item key={item.id}>
						<List.Item.Meta
							title={parseAction(item)}
							description={moment(item.createdAt).format('DD.MM.YYYY HH:mm')}
						/>
						{/*<div>{JSON.stringify(item, null, 4)}</div>*/}
						{/*<div>{}</div>*/}
					</List.Item>
				)}
			></List>
		</div>
	);
});

export default Actions;
