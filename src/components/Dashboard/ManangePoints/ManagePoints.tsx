import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import RecyclePointStore from '../../../modules/RecyclePointStore';
import { Button, Drawer, Input, Space, Table, Tag } from 'antd';
import { ICoordinates, IPoint, PointStatusNames, PointStaus } from '../../../interfaces/IPoint';
import { fractionsDefault } from '../../../constants/constants';
import { SearchOutlined } from '@ant-design/icons';
// @ts-ignore
import Highlighter from 'react-highlight-words';
import styles from '../Dashboard.scss';
import AdminStore from '../../../modules/AdminStore';
import { IRequest, RequestStatus, RequestStatusNames } from '../../../interfaces/IRequest';
import { Link } from 'react-router-dom';
import Paragraph from 'antd/es/typography/Paragraph';
import { IComplaint } from '../../../interfaces/IComplaint';
import PointPanel from '../../SideBar/PointPanel/PointPanel';

// type Record = Pick<IPoint, 'id' | 'title' | 'fractions' | 'coordinates' | 'available'>;

const columns = [
	{
		title: 'Название',
		dataIndex: 'title',
		key: 'title',
		render: (text: string, record: IPoint) => (
			<a target="_blank" href={`/points/${record.id}`}>
				{text}
			</a>
		),
	},
	{
		title: 'Опубликовано',
		dataIndex: 'available',
		key: 'available',
	},
	{
		title: 'Фракции',
		dataIndex: 'fractions',
		key: 'fractions',
		render: (fractions: number[]) => (
			<>
				{fractions.map((id) => {
					const fraction = fractionsDefault.find((f) => f.id === id);

					return (
						<Tag color={fraction.color} key={id}>
							{fraction.name}
						</Tag>
					);
				})}
			</>
		),
	},
	{
		title: 'Дейсвия',
		key: 'action',
		render: (text: string, record: IPoint) => (
			<Space size="middle">
				<a>Снять спубликации</a>
				<a>Удалить</a>
			</Space>
		),
	},
];

const rowSelection = {
	onChange: (selectedRowKeys: any, selectedRows: any) => {
		console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
	},
	getCheckboxProps: (record: IPoint | any) => ({
		// disabled: record.id === 1, // Column configuration not to be checked
		// id: record.id,
		name: record.name,
	}),
};

const ManagePoints = observer(() => {
	const [selectedRowKeys, setSelectedRowKeys] = useState<number[]>([]);
	const [loading, setLoading] = useState<boolean>(false);
	const [loadingUnpublish, setLoadingUnpublish] = useState<boolean>(false);
	const [searchText, setSearchText] = useState<string>('');
	const [searchedColumn, setSearchedColumn] = useState<string>('');
	const { publishPoints, unpublishPPoints, getFullPoints, fullPoints: points } = AdminStore;
	const [filteredInfo, setFilteredInfo] = useState<any>(null);
	const [drawerVisible, setDrawerVisible] = useState(false);
	const [selectedItemLocal, setSelectedItemLocal] = useState<IPoint>(null);
	const { selectPoint } = RecyclePointStore;

	const hasSelected = selectedRowKeys.length > 0;

	useEffect(() => {
		getFullPoints();
	}, []);

	const handleChange = (pagination: any, filters: any, sorter: any) => {
		console.log('Various parameters', pagination, filters, sorter);

		setFilteredInfo(filters);
	};

	const onSelectChange = (selectedRowKeys1: number[]) => {
		console.log('selectedRowKeys changed: ', selectedRowKeys);
		setSelectedRowKeys(selectedRowKeys1);
	};

	const handlePublish = () => {
		setLoading(true);

		publishPoints(selectedRowKeys).then(() => {
			setSelectedRowKeys([]);
			setLoading(false);
			getFullPoints();
		});
	};

	const handleUnpublish = () => {
		setLoadingUnpublish(true);

		unpublishPPoints(selectedRowKeys).then(() => {
			setSelectedRowKeys([]);
			setLoadingUnpublish(false);
			getFullPoints();
		});
	};

	let searchInput: any;

	const getColumnSearchProps = (dataIndex: any) => ({
		filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }: any) => (
			<div style={{ padding: 8 }}>
				<Input
					ref={(node) => {
						searchInput = node;
					}}
					placeholder={`Поиск по ${dataIndex}`}
					value={selectedKeys[0]}
					onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
					onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
					style={{ width: 188, marginBottom: 8, display: 'block' }}
				/>
				<Space>
					<Button
						type="primary"
						onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
						icon={<SearchOutlined />}
						size="small"
						style={{ width: 90 }}
					>
						Поиск
					</Button>
					<Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
						Сбросить
					</Button>
				</Space>
			</div>
		),
		filterIcon: (filtered: any) => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
		onFilter: (value: any, record: any) =>
			record[dataIndex] ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()) : '',
		onFilterDropdownVisibleChange: (visible: boolean) => {
			if (visible) {
				setTimeout(() => searchInput.select(), 100);
			}
		},
		render: (text: string, record: IPoint) =>
			searchedColumn === dataIndex ? (
				<Highlighter
					highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
					searchWords={[searchText]}
					autoEscape
					textToHighlight={
						text ? (
							<a target="_blank" href={`/point/${record.id}`}>
								{text}
							</a>
						) : (
							''
						)
					}
				/>
			) : (
				text
			),
	});

	const handleSearch = (selectedKeys: any, confirm: any, dataIndex: any) => {
		confirm();
		setSearchText(selectedKeys[0]);
		setSearchedColumn(dataIndex);
	};

	const handleReset = (clearFilters: any) => {
		clearFilters();
		setSearchText('');
	};

	const showDrawer = (item: IPoint) => {
		setSelectedItemLocal(item);
		selectPoint(item);
		setDrawerVisible(true);
	};

	const onCloseDrawer = () => {
		setDrawerVisible(false);
	};

	const columns = [
		{
			title: '#',
			dataIndex: 'id',
			key: 'id',
			render: (id: string, record: IPoint) => (
				<a target="_blank" href={`/point/${record.id}`}>
					{id}
				</a>
			),
		},
		{
			title: 'Название',
			dataIndex: 'title',
			key: 'title',
			render: (text: string, record: IPoint) => (
				<a target="_blank" href={`/point/${record.id}`}>
					{text}
				</a>
			),
			...getColumnSearchProps('title'),
		},
		{
			title: 'Адрес',
			dataIndex: 'address',
			key: 'address',
			render: (text: string, record: IPoint) => <span>{text}</span>,
			...getColumnSearchProps('address'),
		},

		{
			title: 'Статус',
			dataIndex: 'status',
			key: 'status',
			filters: [
				{ text: PointStatusNames[PointStaus.PUBLISHED], value: PointStaus.PUBLISHED },
				{ text: PointStatusNames[PointStaus.FROZEN], value: PointStaus.FROZEN },
			],
			filteredValue: filteredInfo?.status || null,
			onFilter: (value: PointStaus, record: IPoint) => value === record.status,
			render: (status: PointStaus, record: IPoint) => <span>{PointStatusNames[status]}</span>,
		},
		{
			title: 'Фракции',
			dataIndex: 'fractions',
			key: 'fractions',
			filters: fractionsDefault.map((fraction) => {
				return {
					text: fraction.name,
					value: fraction.id,
				};
			}),
			filteredValue: filteredInfo?.fractions || null,
			onFilter: (value: number, record: IPoint) => record.fractions.includes(value),
			render: (fractions: number[]) => (
				<>
					{fractions.map((id) => {
						const fraction = fractionsDefault.find((f) => f.id === id);

						return (
							<Tag color={fraction.color} key={id}>
								{fraction.name}
							</Tag>
						);
					})}
				</>
			),
		},
		// {
		// 	title: 'Дейсвия',
		// 	key: 'action',
		// 	render: (text: string, record: IPoint) => (
		// 		<Space size="middle">
		// 			<a>Снять спубликации</a>
		// 			{/*<a>Удалить</a>*/}
		// 		</Space>
		// 	),
		// },
	];

	return (
		<div>
			<Space direction="vertical" className={styles.fullWidth}>
				<Space>
					<Button type="primary" onClick={handlePublish} disabled={!hasSelected} loading={loading}>
						Опубликовать
					</Button>

					<Button type="primary" onClick={handleUnpublish} disabled={!hasSelected} loading={loadingUnpublish}>
						Снять с публикации
					</Button>
				</Space>
				<div>
					<span style={{ marginLeft: 8 }}>
						{hasSelected ? `Выбрано ${selectedRowKeys.length} элементов` : ''}
					</span>
				</div>

				<Table
					dataSource={points?.map((el: any) => {
						el.key = el.id;
						return el;
					})}
					columns={columns}
					rowSelection={{
						type: 'checkbox',
						...rowSelection,
						onChange: onSelectChange,
					}}
					loading={!points}
					onChange={handleChange}
					onRow={(record, rowIndex) => {
						return {
							onClick: (event) => {
								showDrawer(record);
							},
						};
					}}
					rowClassName={styles.antTableRow}
				/>
			</Space>

			<Drawer width={400} placement="right" closable={false} onClose={onCloseDrawer} visible={drawerVisible}>
				{selectedItemLocal && (
					<div>
						<PointPanel request={selectedItemLocal as any} />
					</div>
				)}
			</Drawer>
		</div>
	);
});

export default ManagePoints;
