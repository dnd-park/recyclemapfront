import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { Button, Input, Space, Table, Tag } from 'antd';
import { fractionsDefault } from '../../../constants/constants';
import { SearchOutlined } from '@ant-design/icons';
// @ts-ignore
import Highlighter from 'react-highlight-words';
import UserStore from '../../../modules/UserStore';
import styles from './../Dashboard.scss';
import AdminStore from '../../../modules/AdminStore';
import { IRequest, RequestStatus, RequestStatusNames } from '../../../interfaces/IRequest';

// type Record = Pick<IPoint, 'id' | 'title' | 'fractions' | 'coordinates' | 'available'>;

const ManageRequests = observer(() => {
	const [searchText, setSearchText] = useState<string>('');
	const [searchedColumn, setSearchedColumn] = useState<string>('');
	const [selectedRowKeys, setSelectedRowKeys] = useState<number[]>([]);
	const [loading, setLoading] = useState<boolean>(false);
	const [loadingDeny, setLoadingDeny] = useState<boolean>(false);
	const { approveRequests, denyRequests } = AdminStore;
	const [filteredInfo, setFilteredInfo] = useState<any>(null);
	const [sortedInfo, setSortedInfo] = useState<any>(null);

	const { allRequests, getAllRequests } = UserStore;

	const hasSelected = selectedRowKeys.length > 0;
	let searchInput: any;

	useEffect(() => {
		getAllRequests();
	}, []);

	const handleChange = (pagination: any, filters: any, sorter: any) => {
		console.log('Various parameters', pagination, filters, sorter);

		setFilteredInfo(filters);
		setSortedInfo(sorter);
	};

	const clearFilters = () => {
		setFilteredInfo(null);
	};

	const clearAll = () => {
		setFilteredInfo(null);
		setSortedInfo(null);
	};

	const onSelectChange = (selectedRowKeys1: number[]) => {
		console.log('selectedRowKeys changed: ', selectedRowKeys);
		setSelectedRowKeys(selectedRowKeys1);
	};

	const handleApprove = () => {
		setLoading(true);

		approveRequests(selectedRowKeys)
			.then(() => {
				setLoading(false);
				setSelectedRowKeys([]);
				getAllRequests();
			})
			.catch(() => {
				setLoading(false);
			});
	};

	const handleDeny = () => {
		setLoadingDeny(true);

		denyRequests(selectedRowKeys)
			.then(() => {
				setLoadingDeny(false);
				setSelectedRowKeys([]);
				getAllRequests();
			})
			.catch(() => {
				setLoadingDeny(false);
			});
	};

	const deny = (id: number) => {
		denyRequests([id]).then(() => {
			getAllRequests();
		});
	};

	const approve = (id: number) => {
		approveRequests([id]).then(() => {
			getAllRequests();
		});
	};

	const getColumnSearchProps = (dataIndex: any) => ({
		filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }: any) => (
			<div style={{ padding: 8 }}>
				<Input
					ref={(node) => {
						searchInput = node;
					}}
					placeholder={`Поиск по ${dataIndex}`}
					value={selectedKeys[0]}
					onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
					onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
					style={{ width: 188, marginBottom: 8, display: 'block' }}
				/>
				<Space>
					<Button
						type="primary"
						onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
						icon={<SearchOutlined />}
						size="small"
						style={{ width: 90 }}
					>
						Поиск
					</Button>
					<Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
						Сбросить
					</Button>
				</Space>
			</div>
		),
		filterIcon: (filtered: any) => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
		onFilter: (value: any, record: any) =>
			record[dataIndex] ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()) : '',
		onFilterDropdownVisibleChange: (visible: boolean) => {
			if (visible) {
				setTimeout(() => searchInput.select(), 100);
			}
		},
		render: (text: string) =>
			searchedColumn === dataIndex ? (
				<Highlighter
					highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
					searchWords={[searchText]}
					autoEscape
					textToHighlight={text ? text.toString() : ''}
				/>
			) : (
				text
			),
	});

	const handleSearch = (selectedKeys: any, confirm: any, dataIndex: any) => {
		confirm();
		setSearchText(selectedKeys[0]);
		setSearchedColumn(dataIndex);
	};

	const handleReset = (clearFilters: any) => {
		clearFilters();
		setSearchText('');
	};

	const columns = [
		{
			title: 'Название',
			dataIndex: 'title',
			key: 'title',
			sorter: (a: IRequest, b: IRequest) => a.title.length - b.title.length,
			sortOrder: sortedInfo?.columnKey === 'title' && sortedInfo?.order,
			render: (text: string, record: IRequest) => (
				<a target="_blank" href={`/points/${record.id}`}>
					{text}
				</a>
			),
			...getColumnSearchProps('title'),
		},
		{
			title: 'Адрес',
			dataIndex: 'address',
			key: 'address',
			render: (text: string, record: IRequest) => <span>{text}</span>,
			...getColumnSearchProps('address'),
		},
		{
			title: 'Статус',
			dataIndex: 'status',
			key: 'status',
			filters: [
				{ text: RequestStatusNames[RequestStatus.ON_CHECK], value: RequestStatus.ON_CHECK },
				{ text: RequestStatusNames[RequestStatus.DENIED], value: RequestStatus.DENIED },
				{ text: RequestStatusNames[RequestStatus.APPROVED], value: RequestStatus.APPROVED },
			],
			filteredValue: filteredInfo?.status || null,
			onFilter: (value: RequestStatus, record: IRequest) => value === record.status,
			render: (status: RequestStatus, record: IRequest) => <span>{RequestStatusNames[status]}</span>,
		},
		{
			title: 'Фракции',
			dataIndex: 'fractions',
			key: 'fractions',
			filters: fractionsDefault.map((fraction) => {
				return {
					text: fraction.name,
					value: fraction.id,
				};
			}),
			filteredValue: filteredInfo?.fractions || null,
			onFilter: (value: number, record: IRequest) => record.fractions.includes(value),
			render: (fractions: number[]) => (
				<>
					{fractions.map((id) => {
						const fraction = fractionsDefault.find((f) => f.id === id);

						return (
							<Tag color={fraction.color} key={id}>
								{fraction.name}
							</Tag>
						);
					})}
				</>
			),
		},
		{
			title: 'Дейсвия',
			key: 'action',
			render: (text: string, record: IRequest) => (
				<Space size="middle">
					{record.status === RequestStatus.ON_CHECK && (
						<>
							<a onClick={() => approve(record.id)}>Принять</a>
							<a onClick={() => deny(record.id)}>Отклонить</a>
						</>
					)}
				</Space>
			),
		},
	];

	return (
		<div>
			<div>
				<Space direction="vertical" className={styles.fullWidth}>
					<Space>
						<Button type="primary" onClick={handleApprove} disabled={!hasSelected} loading={loading}>
							Принять заявку
						</Button>
						<Button type="primary" onClick={handleDeny} disabled={!hasSelected} loading={loadingDeny}>
							Отклонить заявку
						</Button>
					</Space>
					<div>
						<span style={{ marginLeft: 8 }}>
							{hasSelected ? `Выбрано ${selectedRowKeys.length} элементов` : ''}
						</span>
					</div>

					<Table
						dataSource={allRequests?.map((el: any) => {
							el.key = el.id;
							return el;
						})}
						columns={columns}
						rowSelection={{
							type: 'checkbox',
							onChange: onSelectChange,
						}}
						loading={!allRequests}
						onChange={handleChange}
					/>
				</Space>
			</div>
		</div>
	);
});

export default ManageRequests;
