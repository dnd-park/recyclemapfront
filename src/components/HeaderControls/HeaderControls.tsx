import React from 'react';
import classNames from 'classnames';
import styles from './HeaderControls.scss';
import { Button, Popconfirm } from 'antd';
import UserControl from '../UserControl/UserControl';
import AuthApp from '../AuthApp/AuthApp';

import { PlusOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react';
import UserStore from '../../modules/UserStore';
import { useHistory } from 'react-router-dom';
import UtilStore from '../../modules/UtilStore';
import { LoginOutlined } from '@ant-design/icons';

const HeaderControls = observer(() => {
	const { isLoggedIn, setAuthModalVisible } = UserStore;
	const { setSidebarOpen } = UtilStore;
	const history = useHistory();

	const openPointForm = () => {
		if (!isLoggedIn) {
			return;
		}

		history.push('/addpoint');
		setSidebarOpen(true);
	};

	const confirm = () => {
		history.push('/addpoint');
		// setAuthModalVisible(true);
	};
	const cancel = () => {};

	const getCreatePointButton = () => {
		const btn = (
			<Button shape="round" icon={<PlusOutlined />} size="large" type={'primary'} onClick={openPointForm}>
				Добавить пункт
			</Button>
		);

		if (isLoggedIn) {
			return btn;
		}

		return (
			<Popconfirm
				title="Хотите добавить пункт приема вторсырья? Тогда войдите в аккаунт"
				placement="leftTop"
				okText="Войти"
				icon={<LoginOutlined />}
				onConfirm={confirm}
				// onCancel={cancel}

				// cancelText="No"
				// visible={!isLoggedIn}
			>
				{btn}
			</Popconfirm>
		);
	};

	return (
		<div className={classNames(styles.Header)}>
			<div className={styles.HeaderItem}>{getCreatePointButton()}</div>
			{isLoggedIn ? (
				<>
					<div className={styles.HeaderItem}>
						<UserControl />
					</div>
				</>
			) : (
				<div className={styles.HeaderItem}>
					<AuthApp match={null} history={null} location={null} />
				</div>
			)}
		</div>
	);
});

export default HeaderControls;
