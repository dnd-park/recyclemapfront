import { Button, Form, Input, Typography } from 'antd';
import React, { useEffect } from 'react';
import Title from 'antd/es/typography/Title';
import { AuthSteps } from '../AuthApp';
import styles from '../AuthApp.scss';
import UserStore from '../../../modules/UserStore';
import { ISignupData } from '../../../interfaces/IUser';
import { observer } from 'mobx-react';
import { useHistory } from 'react-router-dom';

const { Text, Link } = Typography;

interface SignupProps {
	onChangeStep: (step: AuthSteps) => void;
}

const Signup = observer((props: SignupProps) => {
	const { onChangeStep } = props;
	const { signup, error, clearError, setAuthModalVisible } = UserStore;
	let history = useHistory();

	const [form] = Form.useForm();

	useEffect(() => {
		clearError();
		form.resetFields();

		return function cleanup() {
			clearError();
			form.resetFields();
		};
	}, []);

	const onFinish = (values: ISignupData) => {
		signup(values).then(() => {
			// history.push('/profile');
			setAuthModalVisible(false);
		});
	};

	const onFinishFailed = (errorInfo: any) => {
		console.log('Failed:', errorInfo);
	};

	return (
		<Form
			layout="vertical"
			name="basic"
			initialValues={{ remember: true }}
			onFinish={onFinish}
			onFinishFailed={onFinishFailed}
			requiredMark={false}
		>
			<Title level={2}>Создание аккаунта</Title>
			{error && <Text type="danger">{error}</Text>}
			<Form.Item
				label="Имя"
				name="first_name"
				rules={[{ required: true, message: 'Поле "Имя" должно быть заполнено' }]}
			>
				<Input autoComplete="given-name" />
			</Form.Item>
			<Form.Item
				label="Фамилия"
				name="second_name"
				rules={[{ required: true, message: 'Поле "Фамилия" должно быть заполнено' }]}
			>
				<Input autoComplete="family-name" />
			</Form.Item>
			<Form.Item
				label="Электронная почта"
				name="login"
				rules={[{ required: true, message: 'Поле "Электронная почта" должно быть заполнено', type: 'email' }]}
			>
				<Input autoComplete="username" placeholder="Ваша электронная почта" />
			</Form.Item>
			<Form.Item
				label="Пароль"
				name="password"
				rules={[{ required: true, message: 'Поле "Пароль" должно быть заполнено' }]}
			>
				<Input.Password autoComplete="new-password" />
			</Form.Item>

			<div className={styles.ActionRow}>
				<Form.Item>
					<Button type="primary" htmlType="submit" className={styles.ActionRow}>
						Создать
					</Button>
				</Form.Item>
				<Form.Item>
					<Link href="#" onClick={() => onChangeStep(AuthSteps.login)}>
						Уже есть аккаунт
					</Link>
				</Form.Item>
			</div>
			<Text type="secondary">
				Я предоставляю Гринпис в России свои персональные данные и{' '}
				<Link href="http://www.greenpeace.org/russia/ru/help/privacy/" target="_blank">
					подтверждаю
				</Link>{' '}
				свое согласие на их обработку.
			</Text>
		</Form>
	);
});

export default Signup;
