import { Button, Form, Input, Typography } from 'antd';
import React, { useEffect } from 'react';
import { AuthSteps } from '../AuthApp';
import Title from 'antd/es/typography/Title';
import styles from './../AuthApp.scss';
import { observer } from 'mobx-react';
import UserStore from '../../../modules/UserStore';
import { ILoginData } from '../../../interfaces/IUser';
import Text from 'antd/es/typography/Text';
import { useHistory } from 'react-router-dom';
import vkIcon from '../../../icons/VK_Compact_Logo.svg';

const { Link } = Typography;

const layout = {};
const tailLayout = {};

interface LoginProps {
	onChangeStep: (step: AuthSteps) => void;
}

export const Login = observer((props: LoginProps) => {
	const { onChangeStep } = props;
	const { login, error, clearError, isLoggedIn, setAuthModalVisible } = UserStore;

	let history = useHistory();

	const [form] = Form.useForm();

	useEffect(() => {
		clearError();
		form.resetFields();

		return function cleanup() {
			clearError();
			form.resetFields();
		};
	}, []);

	const onFinish = (values: ILoginData) => {
		clearError();
		login(values).then(() => {
			// history.push('/profile');
			setAuthModalVisible(false);
		});
	};

	const onFinishFailed = (errorInfo: any) => {
		console.log('Failed:', errorInfo);
	};

	return (
		<Form
			{...layout}
			layout="vertical"
			name="login"
			initialValues={{ remember: true }}
			onFinish={onFinish}
			onFinishFailed={onFinishFailed}
			requiredMark={false}
		>
			<Title level={2}>Вход</Title>
			{error && <Text type="danger">{error}</Text>}
			<Form.Item
				label="Электронная почта"
				name="login"
				rules={[{ required: true, message: 'Напишите электронную почту', type: 'email' }]}
			>
				<Input placeholder="Электронная почта" autoComplete="username" />
			</Form.Item>
			<Form.Item
				label="Пароль"
				name="password"
				rules={[{ required: true, message: 'Поле "Пароль" должно быть заполнено' }]}
			>
				<Input.Password autoComplete="current-password" placeholder="Пароль" />
			</Form.Item>
			<div className={styles.ActionRow}>
				<Form.Item {...tailLayout}>
					<Button type="primary" htmlType="submit" size="large">
						Войти
					</Button>
				</Form.Item>
				<div style={{ marginBottom: '24px' }}>
					<Link href="#" onClick={() => onChangeStep(AuthSteps.signup)}>
						Создать аккаунт
					</Link>
				</div>
			</div>
			<div>
				<Link href="https://vm417203.eurodir.ru/api/vk/auth">
					<Button size="large" block>
						<img src={vkIcon} style={{ width: '24px', height: '24px', marginRight: '10px' }} />
						<span style={{ lineHeight: '24px' }}>через Вконтакте</span>
					</Button>
				</Link>
			</div>
		</Form>
	);
});

export default Login;
