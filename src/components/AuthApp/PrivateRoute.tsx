import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { observer } from 'mobx-react';
import UserStore from '../../modules/UserStore';

const PrivateRoute = observer(({ component: Component, ...rest }: any) => {
	const { isLoggedIn, canSeeDashboard } = UserStore;

	return (
		<Route
			{...rest}
			render={(props) =>
				canSeeDashboard ? (
					<Component {...props} />
				) : (
					<Redirect
						to={{
							pathname: '/login',
							state: { from: props.location },
						}}
					/>
				)
			}
		/>
	);
});

export default PrivateRoute;
