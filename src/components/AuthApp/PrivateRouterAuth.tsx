import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { observer } from 'mobx-react';
import UserStore from '../../modules/UserStore';

const PrivateRouteAuth = observer(({ component: Component, ...rest }: any) => {
	const { isLoggedIn } = UserStore;

	return (
		<Route
			{...rest}
			render={(props) =>
				isLoggedIn ? (
					<Component {...props} />
				) : (
					<Redirect
						to={{
							pathname: '/login',
							state: { from: props.location },
						}}
					/>
				)
			}
		/>
	);
});

export default PrivateRouteAuth;
