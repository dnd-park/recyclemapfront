import React, { useEffect, useState } from 'react';
import { Button, Col, Modal, Row } from 'antd';
import Login from './Login/Login';
import Signup from './Signup/Signup';
import { LoginOutlined } from '@ant-design/icons';
import { RouteComponentProps, Redirect, useHistory } from 'react-router-dom';
import styles from './AuthApp.scss';
import { observer } from 'mobx-react';
import UserStore from '../../modules/UserStore';

export enum AuthSteps {
	login = 'login',
	signup = 'signup',
}

interface AuthAppProps extends RouteComponentProps {
	mode?: AuthModes;
	step?: AuthSteps;
}

interface AuthAppState {}

export enum AuthModes {
	page = 'page',
	modal = 'modal',
}

const AuthApp = observer((props: AuthAppProps) => {
	const [step, setStep] = useState<AuthSteps>(props.step || AuthSteps.login);

	const history = useHistory();

	const { isLoggedIn, currentUserLoading, authModalVisible, setAuthModalVisible, canSeeDashboard } = UserStore;

	useEffect(() => {
		if (props.mode === AuthModes.page) {
			setAuthModalVisible(true);
		}
	}, []);

	const isPageMode = () => {
		return props.mode === AuthModes.page;
	};

	const showModal = () => {
		setAuthModalVisible(true);
	};

	const handleOk = (e: any) => {
		setAuthModalVisible(false);
	};

	const handleCancel = (e: any) => {
		if (isPageMode()) {
			history.push('/');
			return;
		}

		setAuthModalVisible(false);
	};

	const changeStep = (step: AuthSteps) => {
		setStep(step);
	};

	const getStep = () => {
		switch (step) {
			case AuthSteps.signup:
				return <Signup onChangeStep={changeStep} />;
			case AuthSteps.login:
			default:
				return <Login onChangeStep={changeStep} />;
		}
	};

	const { from } = (props.location?.state as { from: { pathname: string } }) || { from: { pathname: '/' } };
	if (isPageMode() && canSeeDashboard && from.pathname === '/dashboard') {
		return <Redirect to={from} />;
	}

	if (isPageMode() && isLoggedIn && ['/', 'addpoint'].includes(from.pathname)) {
		return <Redirect to={from} />;
	}

	return (
		<>
			{!isPageMode() && (
				<Button shape="round" icon={<LoginOutlined />} size="large" onClick={showModal}>
					Войти
				</Button>
			)}

			<Modal
				centered
				visible={authModalVisible}
				onCancel={handleCancel}
				footer={null}
				bodyStyle={{
					padding: 0,
				}}
			>
				<Row>
					<Col
						md={8}
						sm={0}
						className={step === AuthSteps.signup ? styles.LeftSideSignup : styles.LeftSide}
					></Col>
					<Col md={16} sm={24} className={styles.RightSide}>
						{getStep()}
					</Col>
				</Row>
			</Modal>
		</>
	);
});

export default AuthApp;
