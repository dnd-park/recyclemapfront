import React from 'react';
import { Avatar, Badge } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import styles from './UserControl.scss';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import UtilStore from '../../modules/UtilStore';

const UserControl = observer(() => {
	const { setSidebarOpen } = UtilStore;

	const onClick = () => {
		setSidebarOpen(true);
	};

	return (
		<div className={styles.UserControl}>
			<Link to="/profile" onClick={onClick}>
				<Badge count={null}>
					<Avatar shape="square" size="large" icon={<UserOutlined />} />
				</Badge>
			</Link>
		</div>
	);
});

export default UserControl;
