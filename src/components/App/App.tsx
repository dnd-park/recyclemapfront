import React from 'react';
// import { hot } from 'react-hot-loader';
import styles from './App.scss';
import { observer } from 'mobx-react';
import { Route, Switch } from 'react-router-dom';
import MapApp from '../MapApp/MapApp';
import Dashboard from '../Dashboard/Dashboard';
import AuthApp, { AuthModes, AuthSteps } from '../AuthApp/AuthApp';
import PrivateRoute from '../AuthApp/PrivateRoute';
import UserStore from '../../modules/UserStore';

const App = observer(() => {
	const { currentUserLoading, initialLoaded } = UserStore;

	if (!initialLoaded) {
		return <div>Loading</div>;
	}

	return (
		<div className={styles.App}>
			<Switch>
				<PrivateRoute path="/dashboard" component={Dashboard} />
				<Route path="/login" render={(props) => <AuthApp {...props} mode={AuthModes.page} />} />
				<Route
					path="/signup"
					render={(props) => <AuthApp {...props} mode={AuthModes.page} step={AuthSteps.signup} />}
				/>
				<Route path="/" component={MapApp} />
			</Switch>
		</div>
	);
});

// export default hot(module)(App);
export default App;
