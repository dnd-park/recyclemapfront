import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { Avatar, List, Menu, Dropdown } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import Text from 'antd/es/typography/Text';
import Title from 'antd/es/typography/Title';
import UserStore from '../../../modules/UserStore';
import { Link, Route, Switch, useHistory, useParams } from 'react-router-dom';
import styles from './UserPanel.scss';
import { DownOutlined } from '@ant-design/icons';
import { IComplaint } from '../../../interfaces/IComplaint';
import { IComment } from '../../../interfaces/IComment';
import { IRequest, RequestStatus } from '../../../interfaces/IRequest';
import RecyclePointStore from '../../../modules/RecyclePointStore';
import moment from 'moment';
import { RequestPanel, Requests } from './Requests';
import { Complaints } from './Complaints';
import PointPanel from '../PointPanel/PointPanel';

const UserPanel = observer(() => {
	const { isLoggedIn, user } = UserStore;
	let history = useHistory();

	if (!isLoggedIn) {
		history.push('/');
		return null;
	}

	const onBack = () => {
		history.goBack();
	};

	return (
		<>
			<div className={styles.UserPanel}>
				<div className={styles.UserHead}>
					<Avatar
						// size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 80, xxl: 100 }}
						icon={<UserOutlined />}
						size="large"
						className={styles.Avatar}
					/>
					<div>
						<Title level={5}>
							{user.first_name} {user.second_name}
						</Title>
						{/*<Dropdown overlay={menu}>*/}
						<Text type="secondary" className="ant-dropdown-link">
							{user.login} <DownOutlined />
						</Text>
						{/*</Dropdown>*/}
					</div>
				</div>
				<div>
					<Switch>
						<Route path="/profile/favs" component={Favs} />
						<Route path="/profile/requests/:id" component={RequestPanel} />
						<Route path="/profile/requests" component={Requests} />
						<Route path="/profile/comments" component={Comments} />
						<Route path="/profile/complaints" component={Complaints} />
						<Route path="/profile" component={MainMenu} />
					</Switch>
				</div>
			</div>
		</>
	);
});

export default UserPanel;

const MainMenu = observer(() => {
	const { logout, canSeeDashboard } = UserStore;
	const history = useHistory();

	const handleLogout = () => {
		history.push('/');
		logout();
	};

	return (
		<div className={styles.Links}>
			<Menu style={{ borderRight: 'none' }}>
				{canSeeDashboard && (
					<Menu.Item>
						<Link to={'/dashboard'} className={styles.Link}>
							Перейти в панель управления
						</Link>
					</Menu.Item>
				)}
				<Menu.Item>
					<Link to={'/profile/favs'} className={styles.Link}>
						Избранное
					</Link>
				</Menu.Item>
				{!canSeeDashboard && (
					<Menu.Item>
						<Link to={'/profile/requests'} className={styles.Link}>
							Заявки
						</Link>
					</Menu.Item>
				)}
				<Menu.Item>
					<Link to={'/profile/comments'} className={styles.Link}>
						Комментарии
					</Link>
				</Menu.Item>
				{!canSeeDashboard && (
					<Menu.Item>
						<Link to={'/profile/complaints'} className={styles.Link}>
							Жалобы
						</Link>
					</Menu.Item>
				)}
				<Menu.Item danger>
					<Link to={'/'} className={styles.Link} onClick={handleLogout}>
						Выйти
					</Link>
				</Menu.Item>
			</Menu>
		</div>
	);
});

const Favs = observer(() => {
	const { getFavoritePoints, favoritePoints } = UserStore;

	useEffect(() => {
		getFavoritePoints();
	}, []);

	return (
		<>
			<>
				<List
					itemLayout="horizontal"
					dataSource={favoritePoints}
					renderItem={(item) => (
						<List.Item className={styles.listItem}>
							<Link to={`/point/${item.id}`} className={styles.listItemLink}>
								{item.title}
								<List.Item.Meta description={item.address} />
							</Link>
						</List.Item>
					)}
				/>
			</>
		</>
	);
});

const menu = (
	<Menu>
		<Menu.Item>
			{/*<a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">*/}
			Редактировать данные
			{/*</a>*/}
		</Menu.Item>
		<Menu.Item>
			{/*<a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">*/}
			Изменить пароль
			{/*</a>*/}
		</Menu.Item>
		<Menu.Item>
			{/*<a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">*/}
			Стать модератором
			{/*</a>*/}
		</Menu.Item>
		<Menu.Item danger>Выйти</Menu.Item>
	</Menu>
);

const Comments = observer(() => {
	const { getComments, myComments } = UserStore;

	useEffect(() => {
		getComments().then((c) => {});
	}, []);

	return (
		<>
			<List
				itemLayout="horizontal"
				dataSource={myComments}
				renderItem={(item) => (
					<List.Item className={styles.listItem}>
						<Link to={`/point/${item.point_id}`} className={styles.listItemLink}>
							<Text>{item.comment}</Text>
							<List.Item.Meta description={moment(item.updated).format('DD.MM.YYYY HH:mm')} />
						</Link>
					</List.Item>
				)}
			/>
		</>
	);
});
