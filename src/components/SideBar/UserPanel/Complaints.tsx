import { observer } from 'mobx-react';
import UserStore from '../../../modules/UserStore';
import React, { useEffect, useState } from 'react';
import { Drawer, List } from 'antd';
import Paragraph from 'antd/es/typography/Paragraph';
import styles from './UserPanel.scss';
import { IComplaint } from '../../../interfaces/IComplaint';
import { Link } from 'react-router-dom';

export const Complaints = observer(() => {
	const { getMyComplaints, myComplaints } = UserStore;
	const [drawerVisible, setDrawerVisible] = useState(false);
	const [selectedItem, setSelectedItem] = useState<IComplaint>(null);

	useEffect(() => {
		getMyComplaints();
	}, []);

	const showDrawer = (item: IComplaint) => {
		setSelectedItem(item);
		setDrawerVisible(true);
	};

	const onCloseDrawer = () => {
		setDrawerVisible(false);
	};

	return (
		<>
			<List
				itemLayout="horizontal"
				dataSource={myComplaints}
				renderItem={(item) => (
					<List.Item
						key={item.point_id}
						onClick={() => showDrawer(item)}
						className={styles.listItemClickable}
					>
						<List.Item.Meta
							title={
								<a href={`/point/${item.point_id}`} target="_blank">
									Пункт {item.point_id}
								</a>
							}
							description={
								<Paragraph ellipsis style={{ color: '#ccc', marginBottom: 0 }}>
									{item.description}
								</Paragraph>
							}
						/>
					</List.Item>
				)}
			/>

			<Drawer width={640} placement="right" closable={false} onClose={onCloseDrawer} visible={drawerVisible}>
				{selectedItem && (
					<>
						<p>
							Ваша жалоба к пункту{' '}
							<Link to={`/point/${selectedItem.point_id}`}>{selectedItem.point_id}</Link>
						</p>
						<Paragraph>{selectedItem.description}</Paragraph>
					</>
				)}
			</Drawer>
		</>
	);
});
