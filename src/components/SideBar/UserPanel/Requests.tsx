import { RequestStatus } from '../../../interfaces/IRequest';
import Text from 'antd/es/typography/Text';
import { observer } from 'mobx-react';
import UserStore from '../../../modules/UserStore';
import React, { useEffect, useState } from 'react';
import { Col, Divider, Drawer, List, Row } from 'antd';
import styles from './UserPanel.scss';
import { Link, useParams } from 'react-router-dom';
import RecyclePointStore from '../../../modules/RecyclePointStore';
import PointPanel from '../PointPanel/PointPanel';

const getStatus = (status: RequestStatus) => {
	switch (status) {
		case RequestStatus.APPROVED:
			return <Text type="success">Принята</Text>;
		case RequestStatus.DENIED:
			return <Text type="danger">Отклонена</Text>;
		case RequestStatus.ON_CHECK:
			return <Text type="warning">На проверке</Text>;
	}
};

const DescriptionItem = ({ title, content }: any) => (
	<div className="site-description-item-profile-wrapper">
		<p className="site-description-item-profile-p-label">{title}:</p>
		{content}
	</div>
);

export const Requests = observer(() => {
	const { myRequests, getMyRequests } = UserStore;
	const [drawerVisible, setDrawerVisible] = useState(false);

	useEffect(() => {
		getMyRequests();
	}, []);

	const showDrawer = () => {
		// setDrawerVisible(true);
	};

	const onCloseDrawer = () => {
		setDrawerVisible(false);
	};

	return (
		<>
			<List
				itemLayout="horizontal"
				dataSource={myRequests}
				renderItem={(item) => (
					<Link to={`/profile/requests/${item.id}`}>
						<List.Item
							extra={getStatus(item.status)}
							key={item.id}
							onClick={showDrawer}
							className={styles.listItemClickable}
						>
							<List.Item.Meta
								title={<Link to={`/profile/requests/${item.id}`}>{item.title}</Link>}
								description={item.address}
							/>
						</List.Item>
					</Link>
				)}
			/>

			<Drawer width={640} placement="right" closable={false} onClose={onCloseDrawer} visible={drawerVisible}>
				<p>Лучше показывать в таком же виде как и обычный пункт. А не тут</p>
			</Drawer>
		</>
	);
});

export const RequestPanel = observer(() => {
	const { getMyRequests } = UserStore;
	const { selectPoint } = RecyclePointStore;
	let { id } = useParams<any>();
	const [point, setPoint] = useState(null);

	useEffect(() => {
		getMyRequests().then((reqs) => {
			const point = reqs.find((r) => r.id == id);
			selectPoint(point as any);
			setPoint(point);
		});
	}, []);

	if (!point) {
		return <div>Загрузка...</div>;
	}

	return <PointPanel request={point} />;
});
