import React, { useEffect, useState } from 'react';
import { IPoint, ITimetable, PointStaus } from '../../../interfaces/IPoint';
import styles from './PointPanel.scss';
import { fractionsDefault, icons, weekdaysRusShort } from '../../../constants/constants';
import classNames from 'classnames';
import Title from 'antd/es/typography/Title';
import ComplaintForm from '../ComplaintForm/ComplaintForm';
import { observer } from 'mobx-react';
import RecyclePointStore from '../../../modules/RecyclePointStore';
import {
	Avatar,
	Button,
	Carousel,
	Skeleton,
	Tabs,
	Tooltip,
	Comment,
	Modal,
	Input,
	Rate,
	Popover,
	Form,
	Empty,
} from 'antd';
import { useParams } from 'react-router-dom';
import { Image } from 'antd';
import { hexToRgb } from '../../../utils/hexToRgb';
import moment from 'moment';
const { TabPane } = Tabs;
import {
	EnvironmentOutlined,
	ShareAltOutlined,
	DislikeOutlined,
	LikeOutlined,
	BookOutlined,
	HeartOutlined,
	InfoCircleOutlined,
	LinkOutlined,
	FrownOutlined,
	MehOutlined,
	SmileOutlined,
	UserOutlined,
	ClockCircleOutlined,
} from '@ant-design/icons';
import Text from 'antd/es/typography/Text';
import UserStore, { Request } from '../../../modules/UserStore';
import MapStore from '../../../modules/MapStore';
import Search from 'antd/es/input/Search';
import { useHistory } from 'react-router-dom';
import TextArea from 'antd/es/input/TextArea';
import Paragraph from 'antd/es/typography/Paragraph';
import UtilStore from '../../../modules/UtilStore';
interface Props {
	request?: Request;
}

const messages = {
	fixData: 'Исправить неточность',
};

const customIcons = {
	1: <FrownOutlined />,
	2: <FrownOutlined />,
	3: <MehOutlined />,
	4: <SmileOutlined />,
	5: <SmileOutlined />,
};

const PointPanel = observer((props: Props) => {
	const { selectedPoint, getPoint, clearSelectedPoint, setGradeToPoint } = RecyclePointStore;
	const { setSidebarOpen } = UtilStore;
	const {
		addToFavorites,
		removeFromFavorites,
		favoritePoints,
		isLoggedIn,
		setAuthModalVisible,
		canSeeDashboard,
	} = UserStore;
	let { id } = useParams<any>();
	const { setViewport, viewport } = MapStore;
	const history = useHistory();

	const [shareModalVisible, setShareModalVisible] = useState<boolean>(false);
	const [copied, setCopied] = useState<boolean>(false);
	const [copyText, setCopyText] = useState<string>('Скопировать');
	const [myGrade, setMyGrade] = useState<number>(0);

	useEffect(() => {
		if (props.request) {
			setViewport({
				...viewport,
				longitude: props.request.coordinates.coordinates[0],
				latitude: props.request.coordinates.coordinates[1],
				zoom: 15,
				transitionDuration: 1000,
			});
			return;
		}
		setSidebarOpen(true);
		// if (!selectedPoint) {
		getPoint(Number(id))
			.then((p) => {
				if (p.status !== PointStaus.PUBLISHED && !canSeeDashboard) {
					history.push('/');
					return;
				}

				setViewport({
					...viewport,
					longitude: p.coordinates.coordinates[0],
					latitude: p.coordinates.coordinates[1],
					zoom: 15,
					transitionDuration: 1000,
				});
			})
			.catch(() => {
				history.push('/');
			});
		// }

		setMyGrade(0);
	}, [id]);

	useEffect(() => {
		return () => {
			clearSelectedPoint();
		};
	}, []);

	const isFavorite = () => {
		return favoritePoints.find((point) => point.id === selectedPoint.id);
	};

	if (!selectedPoint) {
		return (
			<div className={styles.row}>
				<Skeleton />
			</div>
		);
	}

	const onTabClick = () => {};

	const handleFavorite = () => {
		if (!isLoggedIn) {
			setAuthModalVisible(true);
			return;
		}

		if (isFavorite()) {
			removeFromFavorites(selectedPoint.id);
			return;
		}

		addToFavorites(selectedPoint.id);
	};

	const handleShareClick = () => {
		navigator.clipboard.writeText(window.location.href);
		setCopied(true);
		setCopyText('Скопировано');
		setTimeout(() => {
			setCopied(false);
			setCopyText('Скопировать');
		}, 1000);
	};

	const { title, description, address, fractions, images, group, timetable } = selectedPoint;

	const isFav = favoritePoints.find((el) => el.id === selectedPoint.id);

	const handleRate = (rate: number) => {
		if (!isLoggedIn) {
			setAuthModalVisible(true);
			return;
		}

		setGradeToPoint({
			pointID: selectedPoint.id,
			grade: rate,
		}).then(() => {
			setMyGrade(rate);
		});
	};

	return (
		<div className={styles.PointPanel}>
			{images && (
				<Carousel>
					{images.map((img) => {
						return (
							<Image
								width={'100%'}
								height={'200px'}
								src={img}
								placeholder={true}
								fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
							/>
						);
					})}
				</Carousel>
			)}

			<div className={classNames(styles.row, styles.mainInfo)}>
				<Title level={3} className={styles.header}>
					{title}
				</Title>

				<div className={styles.fractionsWrapper}>
					{fractions.map((id: number) => {
						const fraction = fractionsDefault.find((el) => el.id === id);
						const { r, g, b } = hexToRgb(fraction.color);
						const color = `rgba(${r}, ${g}, ${b}, 0.7)`;
						return (
							<Tooltip title={fraction.name}>
								<img
									key={id}
									src={fraction.icon}
									alt=""
									className={styles.smallFraction}
									style={{ backgroundColor: color }}
								/>
							</Tooltip>
						);
					})}
				</div>
			</div>
			{!props.request && (
				<div className={styles.row}>
					<div className={styles.actionRow}>
						<div>
							<Tooltip
								title={isFav ? 'Убрать из избранного' : 'Добавить в избранное'}
								placement="topLeft"
							>
								<Button
									icon={<HeartOutlined />}
									shape="circle"
									className={styles.leftAction}
									type={isFav ? 'primary' : 'default'}
									onClick={handleFavorite}
								/>
							</Tooltip>
							<Tooltip title={'Поделиться ссылкой'} placement="topLeft">
								<Button
									icon={<ShareAltOutlined />}
									shape="circle"
									className={styles.leftAction}
									onClick={() => setShareModalVisible(true)}
								/>
							</Tooltip>
						</div>
						<div>
							<Popover
								content={
									<Rate
										// defaultValue={0}
										value={myGrade}
										character={({ index }: { index: number }) => {
											// @ts-ignore
											return customIcons[index + 1];
										}}
										onChange={handleRate}
									/>
								}
								title="Оцените пункт"
							>
								<div>
									<Rate disabled allowHalf value={selectedPoint.grade} />
								</div>
							</Popover>
						</div>
					</div>
				</div>
			)}
			<div className={styles.row}>
				<Tabs defaultActiveKey="1" onChange={onTabClick}>
					<TabPane tab="Инфо" key="1">
						{address && (
							<div className={styles.desc}>
								<div className={styles.desc}>
									<EnvironmentOutlined className={styles.icon} />
									<Text>{address}</Text>
								</div>
							</div>
						)}
						{timetable && (
							<div className={styles.desc}>
								<div className={styles.desc}>
									<ClockCircleOutlined className={styles.icon} />
									<Timetable timetable={timetable} />
								</div>
							</div>
						)}
						{description && (
							<div className={styles.desc}>
								{/*<img className={styles.icon} src={icons.info} />*/}
								<InfoCircleOutlined className={styles.icon} />
								<Paragraph
									// expandable: description.length > 10, иначе мерцает :)
									ellipsis={{ rows: 4, expandable: description.length > 10, symbol: 'раскрыть' }}
								>
									{description}
								</Paragraph>
							</div>
						)}
						{!canSeeDashboard && !props.request && (
							<div className={classNames(styles.center, styles.row)}>
								<ComplaintForm />
							</div>
						)}
					</TabPane>
					{!props.request && (
						<TabPane tab="Комментарии" key="2">
							<Comments />
						</TabPane>
					)}
				</Tabs>
			</div>
			<Modal
				centered
				visible={shareModalVisible}
				onCancel={() => setShareModalVisible(false)}
				footer={null}
				title="Отправить ссылку"
			>
				<Input
					size="large"
					placeholder="large size"
					prefix={<LinkOutlined />}
					value={window.location.href}
					disabled={copied}
					suffix={
						<Button type="primary" onClick={handleShareClick} disabled={copied}>
							{copyText}
						</Button>
					}
				/>
			</Modal>
		</div>
	);
});

export default PointPanel;

const ExampleComment = ({ children, comment }: any) => (
	<Comment
		// actions={[<span key="comment-nested-reply-to">Ответить</span>]}
		author={<a>{comment?.author?.firstName + comment?.author?.secondName}</a>}
		avatar={comment?.avatar ? <Avatar src={comment.avatar} alt={comment.author} /> : <UserOutlined />}
		content={<p>{comment?.comment}</p>}
	>
		{children}
	</Comment>
);

const Editor = ({
	onChange,
	onSubmit,
	submitting,
	value,
}: {
	onChange: (data: any) => void;
	onSubmit: (data: any) => void;
	submitting: boolean;
	value: string;
}) => (
	<>
		<Form.Item>
			<TextArea rows={4} onChange={onChange} value={value} />
		</Form.Item>
		<Form.Item>
			<Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
				Добавить комментарий
			</Button>
		</Form.Item>
	</>
);

const Comments = observer(() => {
	const { getComments, addComment, selectedPoint, selectedPointComments } = RecyclePointStore;
	const { user } = UserStore;
	const [value, setValue] = useState('');
	const [submitting, setSubmitting] = useState<boolean>(false);
	let { id } = useParams<any>();

	useEffect(() => {
		getComments(id).then((comments) => {});
	}, [id]);

	const handleChange = (e: any) => {
		setValue(e.target.value);
	};

	const handleSubmit = () => {
		if (!value) {
			return;
		}

		setSubmitting(true);

		addComment({
			comment: value,
		}).then(() => {
			setSubmitting(false);
			setValue('');
		});
	};

	return (
		<>
			{selectedPointComments.map((comment) => {
				return <ExampleComment comment={comment} key={comment.id} />;
			})}

			{!selectedPointComments?.length && !user && <Empty />}

			{user && (
				<Comment
					avatar={user.avatar ? <Avatar src={user.avatar} alt={user.first_name} /> : <UserOutlined />}
					content={
						<Editor onChange={handleChange} onSubmit={handleSubmit} submitting={submitting} value={value} />
					}
				/>
			)}
		</>
	);
});

const Timetable = ({ timetable }: { timetable: ITimetable }) => {
	if (!timetable) {
		return null;
	}

	if (timetable.isRoundTheClock) {
		return <div>Круглосуточно</div>;
	}

	const { weekdays, breakStartTime, breakEndTime, isBreak } = timetable;

	// make Monday = 0
	const today = (new Date().getDay() || 7) - 1;

	console.log('Timetable', today);
	return (
		<div>
			{weekdays.map((day, index) => {
				return (
					<div style={{ color: index === today ? '#65cd00' : '' }}>
						{weekdaysRusShort[index]} - {moment(day.startTime, ['h:mmA']).format('HH:mm')} -{' '}
						{moment(day.endTime, ['h:mmA']).format('HH:mm')}
					</div>
				);
			})}

			{isBreak && breakStartTime && breakEndTime && (
				<div>
					Перерыв: {moment(breakStartTime, ['h:mmA']).format('HH:mm')} -
					{moment(breakEndTime, ['h:mmA']).format('HH:mm')}
				</div>
			)}
		</div>
	);
};
