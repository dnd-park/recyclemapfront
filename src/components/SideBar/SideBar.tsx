import React, { useState } from 'react';
import styles from './styles.scss';
import { AutoComplete, Button, Input } from 'antd';
import { IPoint } from '../../interfaces/IPoint';
import PointPanel from './PointPanel/PointPanel';
import FractionPanel from './FractionPanel/FractionPanel';
import PointForm from './PointForm/PointForm';
import { observer } from 'mobx-react';
import RecyclePointStore from '../../modules/RecyclePointStore';
import { Route, Switch, useHistory, useLocation } from 'react-router-dom';
import Dashboard from '../Dashboard/Dashboard';
import MapApp from '../MapApp/MapApp';
import UserPanel from './UserPanel/UserPanel';
import classNames from 'classnames';
import { CaretLeftFilled, CaretRightFilled, CloseOutlined, ArrowLeftOutlined } from '@ant-design/icons';
import UtilStore from '../../modules/UtilStore';
import PrivateRoute from '../AuthApp/PrivateRoute';
import PrivateRouteAuth from '../AuthApp/PrivateRouterAuth';
import MapStore from '../../modules/MapStore';

const { Search } = Input;

interface SideBarProps {}

const renderTitle = (title: string) => {
	return (
		<span>
			{title}
			{/*<a*/}
			{/*	style={{ float: 'right' }}*/}
			{/*	href="https://www.google.com/search?q=antd"*/}
			{/*	target="_blank"*/}
			{/*	rel="noopener noreferrer"*/}
			{/*>*/}
			{/*	more*/}
			{/*</a>*/}
		</span>
	);
};

const renderItem = (title: string, data: any) => {
	return {
		value: title,
		data,
		label: (
			<div
				style={{
					display: 'flex',
					justifyContent: 'space-between',
					whiteSpace: 'normal',
				}}
			>
				{title}
				<span>{/*<CaretRightFilled /> {count}*/}</span>
			</div>
		),
	};
};

// const options = [
// 	{
// 		label: renderTitle('Адреса'),
// 		options: [renderItem('AntDesign', 10000), renderItem('AntDesign UI', 10600)],
// 	},
// 	{
// 		label: renderTitle('Пункты'),
// 		options: [renderItem('AntDesign UI FAQ', 60100), renderItem('AntDesign FAQ', 30010)],
// 	},
// ];

const SideBar = observer((props: SideBarProps) => {
	const { selectedPoint } = RecyclePointStore;
	const { sidebarOpen, setSidebarOpen, mapboxSuggestions } = UtilStore;
	const { setViewport } = MapStore;

	const [options, setOptions] = useState<any>([]);
	const history = useHistory();
	const location = useLocation();

	const onSearch = (value: string) => {
		console.log('onSearch', value);
		mapboxSuggestions(value).then((res) => {
			console.log(res);
			const results = res.features.map((f: any) => {
				return renderItem(f.place_name, f);
			});

			const addressOption = {
				label: renderTitle('Адреса'),
				options: results,
			};

			console.log(addressOption);

			setOptions(addressOption.options.length ? [addressOption] : []);
		});
	};

	const handleSearchChange = (e: any) => {
		const { value } = e.target;
		console.log('handleSearchChange', value);
		onSearch(value);
	};

	const handleAutoCompleteChange = (value: string, option: any) => {
		const { center } = option.data;

		setViewport({
			longitude: center[0],
			latitude: center[1],
			transitionDuration: 500,
			zoom: 15,
		});
	};

	const toggleSidebar = () => {
		setSidebarOpen(!sidebarOpen);
	};

	const canGoBack = () => {
		return location.pathname !== '/';
	};

	const onBack = () => {
		if (canGoBack()) {
			history.goBack();
		}
	};

	const closeContent = () => {
		history.replace('/');
		setSidebarOpen(true);
	};

	return (
		<aside
			className={classNames(styles.SideBar, {
				[styles.leftHiddenSidebar]: !sidebarOpen,
			})}
		>
			<div>
				{canGoBack() && (
					<div className={styles.sidebarCloseButton} onClick={closeContent}>
						{<CloseOutlined />}
					</div>
				)}

				<div className={styles.sidebarToggleButton} onClick={toggleSidebar}>
					{sidebarOpen ? <CaretLeftFilled /> : <CaretRightFilled />}
				</div>
			</div>

			<div className={styles.panelContainer}>
				<div
					// className={styles.SearchWrapper}
					style={{
						position: 'absolute',
						left: 0,
						top: 0,
						width: '100%',
						padding: '10px 20px',
						display: 'flex',
					}}
				>
					<AutoComplete
						dropdownClassName={styles.certainCategorySearchDropdown}
						dropdownMatchSelectWidth={250}
						style={{ width: '100%' }}
						options={options}
						onSelect={handleAutoCompleteChange}
					>
						<Search
							placeholder="   Введите Ваш адрес"
							onSearch={onSearch}
							onChange={handleSearchChange}
							// className={styles.Search}
							style={{
								width: '100%',
							}}
							size="large"
							prefix={canGoBack() && <ArrowLeftOutlined onClick={onBack} />}
						/>
					</AutoComplete>
				</div>
				<div style={{ height: '56px' }}></div>

				<Switch>
					<Route path="/point/:id" component={PointPanel} />
					<Route path="/profile" component={UserPanel} />
					<PrivateRouteAuth path="/addpoint" component={PointForm} />
					<Route path="/" component={FractionPanel} />
				</Switch>
			</div>
		</aside>
	);
});

export default SideBar;
