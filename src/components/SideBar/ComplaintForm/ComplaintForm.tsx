import { Button, Col, Form, Input, Modal, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import Text from 'antd/es/typography/Text';
import { observer } from 'mobx-react';
import UserStore from '../../../modules/UserStore';
import RecyclePointStore from '../../../modules/RecyclePointStore';

const ComplaintForm = observer(() => {
	const [visible, setVisible] = useState<boolean>();
	const [form] = Form.useForm();
	const { addComplaint, isLoggedIn, user } = UserStore;
	const { selectedPoint } = RecyclePointStore;

	useEffect(() => {
		form.resetFields();

		return () => {
			form.resetFields();
		};
	}, []);

	const onFinish = (values: any) => {
		console.log('Success:', values);

		if (selectedPoint?.id) {
			addComplaint(selectedPoint.id, {
				email: user.login,
				...values,
			});
		}

		setVisible(false);
	};

	const onFinishFailed = (errorInfo: any) => {
		console.log('Failed:', errorInfo);
	};

	return (
		<>
			<Button shape="round" size="large" onClick={() => setVisible(true)}>
				Исправить неточность
			</Button>
			<Modal centered visible={visible} onCancel={() => setVisible(false)} footer={null} title="Жалоба">
				<Text type="secondary">
					Укажите неточность в комментарии, чтобы наши модераторы смогли исправить информацию о пункте.
				</Text>
				<Form
					layout="vertical"
					name="basic"
					initialValues={{ remember: true }}
					onFinish={onFinish}
					onFinishFailed={onFinishFailed}
					requiredMark={false}
					form={form}
				>
					<Form.Item
						label="Сообщение"
						name="description"
						rules={[{ required: true, message: 'Поле "Сообщение" должно быть заполнено' }]}
					>
						<Input.TextArea rows={8} />
					</Form.Item>

					{!isLoggedIn && (
						<Form.Item
							label="Email"
							name="email"
							rules={[{ required: true, message: 'Поле "Email" должно быть заполнено' }]}
						>
							<Input />
						</Form.Item>
					)}

					<Form.Item>
						<Button type="primary" htmlType="submit">
							Отправить
						</Button>
					</Form.Item>
				</Form>
			</Modal>
		</>
	);
});

export default ComplaintForm;
