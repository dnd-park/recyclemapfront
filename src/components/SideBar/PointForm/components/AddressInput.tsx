import { Form } from 'antd';
import { AddressSuggestions } from 'react-dadata';
import styles from '../PointForm.scss';
import { dadataApiKey } from '../../../../constants/constants';
import React, { ChangeEvent, useEffect, useRef, useState } from 'react';
import MapStore from '../../../../modules/MapStore';
import UtilStore from '../../../../modules/UtilStore';
import { observer } from 'mobx-react';

export const AddressInput = observer(() => {
	const { setNewPointLngLat, setViewport } = MapStore;
	const { dadataInput, setDadataInput } = UtilStore;

	const suggestionsRef = useRef<AddressSuggestions>(null);

	useEffect(() => {
		if (!dadataInput) {
			return;
		}

		console.log('dadataInput', dadataInput);
		suggestionsRef.current.setInputValue(dadataInput.value);

		return () => {
			setDadataInput(null);
		};
	}, [dadataInput]);

	const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
		const { value } = event.target;
		setDadataInput({
			value,
		});
	};

	return (
		<>
			<Form.Item
				label="Адрес"
				name="address"
				rules={[{ required: !Boolean(dadataInput), message: 'Поле "Адрес" должно быть заполнено' }]}
				help={'Напишите адрес. Кликните на нужную точку на карте чтобы задать расположение пункта'}
			>
				<AddressSuggestions
					inputProps={{ className: 'ant-input', onChange: handleInputChange }}
					containerClassName={styles.reactDadata__container}
					suggestionsClassName={styles.reactDadata__suggestions}
					suggestionClassName={styles.reactDadata__suggestion}
					currentSuggestionClassName={styles.reactDadata__suggestion_current}
					hintClassName={styles.reactDadata__suggestion_subtitle}
					highlightClassName={styles.reactDadata__highlighted}
					ref={suggestionsRef}
					token={dadataApiKey}
					value={dadataInput}
					delay={100}
					count={5}
					minChars={2}
					onChange={(sug: any) => {
						console.log('onChange sug', sug);
						// console.log(dadataInput);
						setNewPointLngLat([Number(sug.data.geo_lon), Number(sug.data.geo_lat)]);
						setViewport({
							longitude: Number(sug.data.geo_lon),
							latitude: Number(sug.data.geo_lat),
							transitionDuration: 500,
							zoom: 13,
						});
						setDadataInput(sug);
						// setAddressInput(suggestionsRef.current.state.inputQuery);
					}}
				/>
			</Form.Item>
		</>
	);
});
