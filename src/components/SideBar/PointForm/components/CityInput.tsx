import { Form, Select } from 'antd';
import React, { useRef, useState } from 'react';
import { observer } from 'mobx-react';
import UtilStore from '../../../../modules/UtilStore';
import MapStore from '../../../../modules/MapStore';

const { Option } = Select;

export const CityInput = observer(() => {
	const { cities, dadataSuggest } = UtilStore;
	const { setNewPointLngLat, setViewport } = MapStore;

	const handleChange = (id: number) => {
		const query = 'город ' + cities.find((c) => c.id === id).name;

		dadataSuggest(query).then((res) => {
			const data = res?.suggestions[0]?.data;
			if (!data) {
				return;
			}

			let { geo_lon, geo_lat } = data;
			geo_lon = Number(geo_lon);
			geo_lat = Number(geo_lat);

			setNewPointLngLat([geo_lon, geo_lat]);
			setViewport({
				longitude: geo_lon,
				latitude: geo_lat,
				transitionDuration: 500,
				zoom: 13,
			});
		});
	};

	return (
		<Form.Item
			label="Город"
			name="city"
			rules={[{ required: true, message: 'Поле "Город" должно быть заполнено' }]}
		>
			<Select
				showSearch
				style={{ width: 200 }}
				placeholder="Выберите город"
				optionFilterProp="children"
				filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				onSelect={handleChange}
			>
				{cities.map((city) => (
					<Option value={city.id}>{city.name}</Option>
				))}
			</Select>
		</Form.Item>
	);
});
