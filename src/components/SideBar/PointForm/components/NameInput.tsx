import { Form, Input } from 'antd';
import React from 'react';

export const NameInput = () => {
	return (
		<Form.Item
			label="Название"
			name="title"
			rules={[{ required: true, message: 'Поле "Название" должно быть заполнено' }]}
		>
			<Input />
		</Form.Item>
	);
};
