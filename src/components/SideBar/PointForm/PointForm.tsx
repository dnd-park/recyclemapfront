import Title from 'antd/es/typography/Title';
import { Button, DatePicker, Form, Input, Result, Select, Space, Switch, TimePicker, Upload } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import styles from './PointForm.scss';
import ImgCrop from 'antd-img-crop';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import MapStore, { MapMods } from '../../../modules/MapStore';
import { AddressSuggestions } from 'react-dadata';
import { dadataApiKey, fractionOptions, weekdaysRus } from '../../../constants/constants';
import RecyclePointStore from '../../../modules/RecyclePointStore';
import { IPoint, ITimetable, Weekdays } from '../../../interfaces/IPoint';
import { observer } from 'mobx-react';
import Text from 'antd/es/typography/Text';
import { UploadChangeParam } from 'antd/es/upload';
import { UploadFile } from 'antd/es/upload/interface';
import UserStore from '../../../modules/UserStore';
import UtilStore from '../../../modules/UtilStore';
import { defaultTimetable } from '../../../constants/defaultTimetable';
import moment from 'moment';
import { NameInput } from './components/NameInput';
import { CityInput } from './components/CityInput';
import { AddressInput } from './components/AddressInput';

const { RangePicker: TimeRangePicker } = TimePicker;

const { Option } = Select;
const { RangePicker } = DatePicker;

interface PointForm {}

const PointForm = observer((props: PointForm) => {
	const history = useHistory();
	const [fileList, setFileList] = useState<UploadFile[]>([]);
	const { setMode: setMapMode, setNewPointLngLat, setViewport, newPointLngLat } = MapStore;
	const { insertPoint, createError, uploadPhotos, insertRequest, uploadRequestPhotos } = RecyclePointStore;
	const [form] = Form.useForm();
	const [showResult, setShowResult] = useState<boolean>(false);
	const [pointTitle, setPointTitle] = useState<string>('');
	const { canSeeDashboard } = UserStore;
	const { dadataInput } = UtilStore;
	const [timetable, setTimetable] = useState<ITimetable>(defaultTimetable);

	useEffect(() => {
		setMapMode(MapMods.addPoint);
	}, []);

	useEffect(() => {
		return () => {
			setMapMode(MapMods.showPoints);
			setNewPointLngLat(null);
		};
	}, []);

	const onCancel = () => {
		history.push('/');
	};

	const onFinish = (values: IPoint, lngLat: number[]) => {
		console.log('Success:', values);
		const point: IPoint = {
			images: [],
			group: 1,
			longitude: lngLat[0],
			latitude: lngLat[1],
			...values,
			address: dadataInput?.value,
			createdBy: 1,
			phones: values.phones ? Object.values(values.phones) : [],
			emails: values.emails ? Object.values(values.emails) : [],
			sites: values.sites ? Object.values(values.sites) : [],
			timetable: timetable,
			timestring: JSON.stringify(timetable).split('"').join("'") as any,
		};

		const promise = canSeeDashboard ? insertPoint(point) : insertRequest(point);

		promise
			.then((idData) => {
				const id = idData?.id || idData;
				console.log(id);

				if (fileList?.length) {
					const formData = new FormData();
					fileList.forEach((file) => {
						formData.append('images', file.originFileObj);
					});

					return canSeeDashboard ? uploadPhotos(id, formData) : uploadRequestPhotos(id, formData);
				}
			})
			.then(() => {
				setPointTitle(values.title);
				form.resetFields();
				setShowResult(true);
			});
	};

	const onClickAddAnother = () => {
		setShowResult(false);
	};

	const onEnd = () => {
		history.push('/');
	};

	const onFinishFailed = (errorInfo: any) => {
		console.log('Failed:', errorInfo);
	};

	const onImagesChange = ({ fileList: newFileList }: UploadChangeParam) => {
		console.log(newFileList);
		setFileList([
			...newFileList.map((file) => {
				file.status = 'done';

				return file;
			}),
		]);
	};

	const onImagePreview = async (file: any) => {
		let src = file.url;
		if (!src) {
			src = await new Promise((resolve) => {
				const reader = new FileReader();
				reader.readAsDataURL(file.originFileObj);
				reader.onload = () => resolve(reader.result);
			});
		}
		const image = new Image();
		image.src = src;
		const imgWindow = window.open(src);
		imgWindow.document.write(image.outerHTML);
	};

	// const handleSearch = (value: string) => {
	// 	suggestionsRef.current.setInputValue(value);
	// 	suggestionsRef.current.getLoadSuggestionsData();
	// };
	// const suggestionsRef = useRef<AddressSuggestions>(null);

	const getDescriptionInput = () => {
		return (
			<Form.Item
				label="Описание"
				name="description"
				rules={[{ required: true, message: 'Поле "Описание" должно быть заполнено' }]}
			>
				<Input.TextArea rows={8} />
			</Form.Item>
		);
	};

	const onFloatingChange = (value: boolean) => {
		setTimetable({
			...timetable,
			isFloating: value,
		});
	};

	const getTemporalPointInput = () => {
		return (
			<Form.Item label="Временный пункт" name="temp_point">
				<Space direction="vertical" size={12}>
					<Switch checked={timetable.isFloating} onChange={onFloatingChange} />
					{timetable.isFloating && <RangePicker size="large" />}
				</Space>
			</Form.Item>
		);
	};

	const format = 'HH:mm A';
	const onDaySwitch = (e: boolean, dayKey: Weekdays) => {
		setTimetable({
			...timetable,
			weekdays: timetable.weekdays.map((el) => {
				if (el.name === dayKey) {
					el.work = e;
				}
				return el;
			}),
		});
		console.log('onDaySwitch', e, dayKey);
		console.log('onDaySwitch', timetable);
	};
	const onTimeChange = (e: any, dayKey: Weekdays) => {
		setTimetable({
			...timetable,
			weekdays: timetable.weekdays.map((el) => {
				if (el.name === dayKey) {
					el.startTime = e[0].format(format);
					el.endTime = e[1].format(format);
				}
				return el;
			}),
		});
		console.log('onTimeChange', e, dayKey);
		console.log('onTimeChange', timetable);
	};
	const onBreakTimeChange = (e: any) => {
		console.log('onBreakTimeChange', timetable);
		setTimetable({
			...timetable,
			breakStartTime: e[0].format(format),
			breakEndTime: e[1].format(format),
		});
	};

	const getTimetableInput = () => {
		return (
			<Form.Item label="Расписание" name="schedule">
				<div style={{ backgroundColor: '#cccccc36', padding: '15px' }}>
					{timetable.weekdays.map((day) => {
						return (
							<div>
								<div style={{ display: 'flex', justifyContent: 'space-between' }}>
									<span>{weekdaysRus[day.name]}</span>
									<Switch checked={day.work} onChange={(e) => onDaySwitch(e, day.name)} />
								</div>
								<div>
									<TimeRangePicker
										bordered={false}
										format={format}
										disabled={!day.work}
										defaultValue={[moment(day.startTime, format), moment(day.endTime, format)]}
										onChange={(e) => onTimeChange(e, day.name)}
									/>
								</div>
							</div>
						);
					})}
				</div>
				<Form.Item label="Перерыв" name="schedule" style={{ backgroundColor: '#cccccc36', padding: '15px' }}>
					<TimeRangePicker
						bordered={false}
						format={format}
						defaultValue={[
							moment(timetable.breakStartTime, format),
							moment(timetable.breakEndTime, format),
						]}
						onChange={(e) => onBreakTimeChange(e)}
					/>
				</Form.Item>
			</Form.Item>
		);
	};

	const getPhotosInput = () => {
		return (
			<Form.Item label="Фотографии" name="images">
				<ImgCrop rotate zoom grid modalTitle="Редактировать фото" aspect={4 / 3}>
					<Upload
						// action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
						listType="picture-card"
						fileList={fileList}
						onChange={onImagesChange}
						onPreview={onImagePreview}
						action={(e) => {
							console.log('action', e);
							return '';
						}}
						onDownload={(e) => {
							console.log('onDownload', e);
						}}
						customRequest={(e) => {
							console.log('customRequest', e);
						}}
					>
						{fileList.length < 20 && '+ Загрузить'}
					</Upload>
				</ImgCrop>
			</Form.Item>
		);
	};

	const getFractionsInput = () => {
		return (
			<Form.Item
				label="Что принимает"
				name="fractions"
				rules={[{ required: true, message: 'Нужно выбрать хотя бы одну фракцию' }]}
			>
				<Select
					mode="multiple"
					showArrow
					defaultValue={[]}
					style={{ width: '100%' }}
					optionLabelProp="label"
					options={fractionOptions}
				/>
			</Form.Item>
		);
	};

	// https://github.com/ant-design/ant-design/issues/16404
	const getPhonesInput = () => {
		return (
			<Form.Item
				label="Телефон"
				name="phones"
				// rules={[{ required: true, message: 'Нужно выбрать хотя бы одну фракцию' }]}
			>
				<Form.List name="phones">
					{(fields, { add, remove }) => (
						<>
							<Form.Item
								name={'phone'}
								fieldKey={'phone'}
								// rules={[{ required: true, message: 'Заполните телефон' }]}
							>
								<Input placeholder="Номер телефона" />
							</Form.Item>

							{fields.map((field) => (
								<Space key={field.key} style={{ display: 'flex', marginBottom: 4 }} align="baseline">
									<Form.Item
										{...field}
										name={[field.name, 'phone']}
										fieldKey={[field.fieldKey, 'phone']}
										// rules={[{ required: true, message: 'Заполните телефон' }]}
									>
										<Input placeholder="Номер телефона" />
									</Form.Item>
									<MinusCircleOutlined onClick={() => remove(field.name)} />
								</Space>
							))}
							<Form.Item>
								<Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
									Добавить номер
								</Button>
							</Form.Item>
						</>
					)}
				</Form.List>
			</Form.Item>
		);
	};

	const getEmailsInput = () => {
		return (
			<Form.Item
				label="Email"
				name="emails"
				// rules={[{ required: true, message: 'Нужно выбрать хотя бы одну фракцию' }]}
			>
				<Form.List name="emails">
					{(fields2, { add, remove }) => (
						<>
							<Form.Item
								name={'email'}
								fieldKey={'email'}
								// rules={[{ required: true, message: 'Заполните email' }]}
							>
								<Input placeholder="Email" />
							</Form.Item>

							{fields2.map((field) => (
								<Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
									<Form.Item
										{...field}
										name={[field.name, 'first']}
										fieldKey={[field.fieldKey, 'first']}
										// rules={[{ required: true, message: 'Missing first name' }]}
									>
										<Input placeholder="Email" />
									</Form.Item>
									<MinusCircleOutlined onClick={() => remove(field.name)} />
								</Space>
							))}
							<Form.Item>
								<Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
									Добавить email
								</Button>
							</Form.Item>
						</>
					)}
				</Form.List>
			</Form.Item>
		);
	};

	const getSitesInput = () => {
		return (
			<Form.Item
				label="Сайт"
				name="sites"
				// rules={[{ required: true, message: 'Нужно выбрать хотя бы одну фракцию' }]}
			>
				<Form.List name="sites">
					{(fields3, { add, remove }) => (
						<>
							<Form.Item
								name={'site'}
								fieldKey={'site'}
								// rules={[{ required: true, message: 'Введите ссылку' }]}
							>
								<Input placeholder="Сайт" />
							</Form.Item>

							{fields3.map((field) => (
								<Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
									<Form.Item
										{...field}
										name={[field.name, 'first']}
										fieldKey={[field.fieldKey, 'first']}
										// rules={[{ required: true, message: 'Missing first name' }]}
									>
										<Input placeholder="Ссылка на сайт" />
									</Form.Item>
									<MinusCircleOutlined onClick={() => remove(field.name)} />
								</Space>
							))}
							<Form.Item>
								<Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
									Добавить ссылку
								</Button>
							</Form.Item>
						</>
					)}
				</Form.List>
			</Form.Item>
		);
	};

	const getResult = () => {
		return (
			<Result
				status="success"
				title={canSeeDashboard ? 'Пункт создан' : 'Спасибо, что делаете RecycleMap лучше'}
				subTitle={canSeeDashboard ? pointTitle : 'Когда информацию проверят, вам на почту придет уведомление'}
				extra={[
					<Button type="primary" key="add-another" onClick={onClickAddAnother}>
						Добавить еще пункт
					</Button>,
					<Button key="ready" onClick={onEnd}>
						Готово
					</Button>,
				]}
			/>
		);
	};

	if (showResult) {
		return <div className={styles.PointFormWrapper}>{getResult()}</div>;
	}

	return (
		<div className={styles.PointFormWrapper}>
			<Title level={2}>Создать новый пункт приема</Title>

			<Form
				layout="vertical"
				name="basic"
				initialValues={{ remember: true }}
				onFinish={(values) => onFinish(values, newPointLngLat)}
				onFinishFailed={onFinishFailed}
				requiredMark={false}
			>
				<NameInput />
				<CityInput />
				<AddressInput />
				{getDescriptionInput()}
				{getTemporalPointInput()}
				{getPhotosInput()}
				{getFractionsInput()}
				{getTimetableInput()}
				{getPhonesInput()}
				{getEmailsInput()}
				{getSitesInput()}

				<div style={{ marginBottom: '24px' }}>{createError && <Text type="danger">{createError}</Text>}</div>

				<Form.Item>
					<Space>
						<Button type="primary" htmlType="submit" size="large">
							Создать
						</Button>
						<Button onClick={onCancel} size="large">
							Отмена
						</Button>
					</Space>
				</Form.Item>
			</Form>
		</div>
	);
});

export default PointForm;
