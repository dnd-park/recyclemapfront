import React from 'react';
import styles from './FractionPanel.scss';
import Title from 'antd/es/typography/Title';
import { observer } from 'mobx-react';
import RecyclePointStore from '../../../modules/RecyclePointStore';
import { IFraction } from '../../../interfaces/IFraction';
import classNames from 'classnames';
import Text from 'antd/es/typography/Text';
import { Link } from 'react-router-dom';
import UserStore from '../../../modules/UserStore';
interface Props {}

const messages = {
	header: 'Что вы хотите сдать на переработку?',
};

const FractionPanel = observer((props: Props) => {
	const { fractions, toggleFraction } = RecyclePointStore;
	const { canSeeDashboard } = UserStore;

	const onClick = (fractionId: number) => {
		toggleFraction(fractionId);
	};

	return (
		<div className={styles.fractionPanel}>
			<Title level={3}>{messages.header}</Title>
			<div className={styles.fractionsWrapper}>
				{fractions.map((fraction: IFraction, index) => {
					const style = fraction.selected ? { backgroundColor: fraction.color } : {};
					return (
						<div
							className={classNames(styles.fractionItem)}
							style={style}
							key={fraction.id}
							onClick={() => onClick(fraction.id)}
						>
							<img
								src={fraction.icon}
								alt={fraction.fraction_value}
								className={classNames(styles.fractionItemIcon)}
							/>
							<Text className={styles.fractionItemText}>{fraction.name}</Text>
						</div>
					);
				})}
			</div>

			{canSeeDashboard && (
				<div className={styles.dashboardLink}>
					<Link to="/dashboard">Перейти в панель управления</Link>
				</div>
			)}
		</div>
	);
});

export default FractionPanel;
