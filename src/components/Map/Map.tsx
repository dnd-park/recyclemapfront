import React, { useEffect, useState } from 'react';
import MapGL, { GeolocateControl, Layer, Marker, NavigationControl, Source } from 'react-map-gl';
import { mapBoxAccessToken, mapBoxStyleStreets } from '../../constants/constants';

import markerIcon from '../../icons/recycle-symbol-green-128.png';
import recyclingIcon from '../../icons/recycling-128.png';
import newPointImg from '../../icons/point.svg';
import { IPoint } from '../../interfaces/IPoint';

import styles from './Map.scss';
import { observer } from 'mobx-react';
import RecyclePointStore from '../../modules/RecyclePointStore';
import MapStore, { MapMods } from '../../modules/MapStore';
import { useHistory } from 'react-router-dom';

interface Props {}

// todo: MobX замедлили работу карты?
// https://mobx.js.org/react-optimizations.html
const RecycleMap = observer(({}: Props) => {
	const { points, getAll, geoJSONPoints, selectedPoint, selectPoint, getPoint } = RecyclePointStore;
	const {
		viewport,
		setViewport,
		layerClusterCircle,
		layerClusterCount,
		layerPointCircle,
		layerPoints,
		mode,
		newPointLngLat,
		setNewPointLngLat,
	} = MapStore;
	const history = useHistory();

	const mapRef = React.createRef<any>();
	const sourceRef = React.createRef<any>();

	let lock = false;

	useEffect(() => {
		// if (!newPointLngLat) {
		// 	return;
		// }
		setLocalNewPointLngLat(newPointLngLat);
	}, [newPointLngLat]);

	useEffect(() => {
		if (!points) {
			getAll();
		}

		const listener = (e: any) => {
			if (e.key === 'Escape') {
				selectPoint(null);
			}
		};
		window.addEventListener('keydown', listener);

		return () => {
			window.removeEventListener('keydown', listener);
		};
	}, []);

	const onSelect = (point: IPoint) => {
		selectPoint(point);
	};

	const onMapClick = (e: any) => {
		console.log(e);
		if (mode === MapMods.addPoint) {
			lock = true;
			setLocalNewPointLngLat(e.lngLat);
			setNewPointLngLat(e.lngLat);
			return;
		}

		const feature = e.features[0];
		const [longitude, latitude] = e.lngLat;

		if (feature?.properties.cluster) {
			const mapboxSource = sourceRef.current.getSource();
			const clusterId = feature.properties.cluster_id;

			mapboxSource.getClusterExpansionZoom(clusterId, (err: any, zoom: any) => {
				if (err) {
					return;
				}

				setViewport({
					...viewport,
					longitude: feature.geometry.coordinates[0],
					latitude: feature.geometry.coordinates[1],
					zoom,
					transitionDuration: 500,
				});
			});

			history.push(`/`);
			selectPoint(null);
			return;
		}

		if (!(points && feature?.id)) {
			selectPoint(null);
			return;
		}

		const p = points?.find(({ id }) => Number(id) === feature.id);
		if (!p) {
			return;
		}

		selectPoint(p);
		history.push(`/point/${p.id}`);
		getPoint(p.id);
	};

	const onMapMouseEnter = () => {
		mapRef.current.getMap().getCanvas().style.cursor = 'pointer';
	};

	const onMapMouseLeave = () => {
		mapRef.current.getMap().getCanvas().style.cursor = '';
	};

	const onMapLoad = () => {
		if (!mapRef) return;
		const map = mapRef.current.getMap();
		map.loadImage(markerIcon, (error: any, image: any) => {
			if (error) return;
			map.addImage('markerIcon', image);
		});

		map.loadImage(recyclingIcon, (error: any, image: any) => {
			if (error) return;
			map.addImage('recyclingIcon', image);
		});
	};

	const [localNewPointLngLat, setLocalNewPointLngLat] = useState<number[]>(null);
	const onMapHover = (e: any) => {
		// if (!newPointLngLat && !lock) {
		// 	setLocalNewPointLngLat(e.lngLat);
		// }
	};

	const updateNewPoint = (e: any) => {
		console.log(e);
	};

	const onDragMarker = (e: any) => {
		console.log(e);
	};

	return (
		<div className={styles.MapWrapper}>
			<MapGL
				{...viewport}
				width="100%"
				height="100%"
				mapStyle={mapBoxStyleStreets}
				mapboxApiAccessToken={mapBoxAccessToken}
				ref={mapRef}
				onViewportChange={(viewport) => setViewport(viewport)}
				onLoad={onMapLoad}
				onClick={onMapClick}
				// onNativeClick={onMapClick}
				interactiveLayerIds={mode === MapMods.showPoints ? [layerPoints.id, layerClusterCircle.id] : undefined}
				onMouseEnter={onMapMouseEnter}
				onMouseLeave={onMapMouseLeave}
				onHover={onMapHover}
				minZoom={4}
				maxZoom={21}
			>
				<GeolocateControl
					className={styles.geolocateStyle}
					positionOptions={{ enableHighAccuracy: true }}
					trackUserLocation={true}
					showUserLocation={true}
				/>
				<div style={{ position: 'absolute', right: '10px', bottom: '350px' }}>
					<NavigationControl />
				</div>
				{mode === MapMods.showPoints && Boolean(geoJSONPoints) && (
					<Source
						id="my-data"
						type="geojson"
						data={geoJSONPoints}
						cluster
						clusterMaxZoom={14}
						clusterRadius={50}
						buffer={0}
						ref={sourceRef}
					>
						<Layer
							{...layerPointCircle}
							// filter={['all', ['!', ['has', 'point_count']], ['!', ['has', 'point_count']]]}
						/>
						<Layer
							{...layerPoints}
							// filter={['all', ['!', ['has', 'point_count']]]}
						/>
						<Layer
							{...layerClusterCircle}
							// paint пишу здесь, потому что ругается на типы
							paint={{
								// 'circle-color': ['step', ['get', 'point_count'], '#68a558', 100, '#68a558', 750, '#68a558'],
								'circle-color': '#68a558',
								'circle-radius': ['step', ['get', 'point_count'], 20, 100, 30, 750, 40],
							}}
							// filter={['all', ['has', 'point_count'], fractionFilter]}
						/>
						<Layer
							{...layerClusterCount}
							// filter={['all', ['has', 'point_count'], fractionFilter]}
						/>
					</Source>
				)}

				{selectedPoint && (
					<Marker
						longitude={selectedPoint.coordinates.coordinates[0]}
						latitude={selectedPoint.coordinates.coordinates[1]}
					>
						<div className={styles.markText}>{selectedPoint.title}</div>
					</Marker>
				)}

				{localNewPointLngLat && (
					<Marker
						longitude={localNewPointLngLat[0]}
						latitude={localNewPointLngLat[1]}
						draggable
						onDragEnd={updateNewPoint}
						onDrag={onDragMarker}
					>
						<img className={styles.newPointMark} src={newPointImg} />
					</Marker>
				)}

				{/*{selectedPoint && (*/}
				{/*	<Popup*/}
				{/*		longitude={selectedPoint.Longitude}*/}
				{/*		latitude={selectedPoint.Latitude}*/}
				{/*		onClose={() => {*/}
				{/*			setSelectedPoint(null);*/}
				{/*		}}*/}
				{/*	>*/}
				{/*		<div>{selectedPoint.Title}</div>*/}
				{/*	</Popup>*/}
				{/*)}*/}
			</MapGL>
		</div>
	);
});

export default RecycleMap;
