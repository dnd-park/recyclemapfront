import { observer } from 'mobx-react';
import styles from './Map.scss';
import React, { useEffect, useState } from 'react';
import MapGL, { GeolocateControl, Layer, Marker, Source, NavigationControl, ExtraState } from 'react-map-gl';
import RecyclePointStore from '../../modules/RecyclePointStore';
import MapStore, { MapMods } from '../../modules/MapStore';
import { mapBoxAccessToken, mapBoxStyleStreets } from '../../constants/constants';
// import selectedPointIcon from '../../icons/recycle-symbol-green-64.png';
import selectedPointIcon from '../../icons/recycle-symbol.svg';
import { mvtSource } from './sources';
import { useHistory } from 'react-router-dom';
import { IMarker } from '../../interfaces/IMarker';
import { Tooltip } from 'antd';
import newPointImg from '../../icons/point.svg';
import UtilStore from '../../modules/UtilStore';
// @ts-ignore
import MapboxLanguage from '@mapbox/mapbox-gl-language';

let globalMap: any;

const MvtMap = observer(() => {
	const { selectedPoint, selectPoint, mvtSourceTiles } = RecyclePointStore;
	const { viewport, setViewport, mode, updateMarkers, markersOnScreen, newPointLngLat, setNewPointLngLat } = MapStore;
	const { mapboxReverseGeocode, setDadataInput, dadataSuggest } = UtilStore;

	const history = useHistory();

	const mapRef = React.createRef<any>();
	const sourceRef = React.createRef<any>();
	const [localNewPointLngLat, setLocalNewPointLngLat] = useState<number[]>(null);
	let lock = false;

	useEffect(() => {
		if (!globalMap || mode === MapMods.showPoints) {
			return;
		}

		const cb = (e: any) => {
			if (globalMap.getSource(mvtSource.id) && globalMap.isSourceLoaded(mvtSource.id) && e.isSourceLoaded) {
				console.log('map load sourcedata');
				handleMarkersUpdate(globalMap);
				globalMap.off('sourcedata', cb);
			}
		};

		globalMap.on('sourcedata', cb);
	}, [mode]);

	useEffect(() => {
		setLocalNewPointLngLat(newPointLngLat);
	}, [newPointLngLat]);

	useEffect(() => {
		if (!mapRef.current || !sourceRef.current) {
			return;
		}

		const map = mapRef.current.getMap();
		const source = sourceRef.current.getSource();
		// https://github.com/mapbox/mapbox-gl-js/issues/2941#issuecomment-518631078
		source.tiles = mvtSourceTiles;
		// Remove the tiles for a particular source
		map.style.sourceCaches[source.id].clearTiles();
		// Load the new tiles for the current viewport (map.transform -> viewport)
		map.style.sourceCaches[source.id].update(map.transform);
		map.triggerRepaint();

		const sourceCallback = (e: any) => {
			if (map.getSource(source.id) && map.isSourceLoaded(source.id) && e.isSourceLoaded) {
				console.log('useEffect sourcedata');
				handleMarkersUpdate();
				map.off('sourcedata', sourceCallback);
			}
		};
		map.on('sourcedata', sourceCallback);
	}, [mvtSourceTiles]);

	useEffect(() => {
		const listener = (e: any) => {
			if (e.key === 'Escape') {
				selectPoint(null);
			}
		};
		window.addEventListener('keydown', listener);

		return () => {
			window.removeEventListener('keydown', listener);
		};
	}, []);

	const handleMarkerClick = (e: any, marker: IMarker) => {
		history.push(`/point/${marker.id}`);
	};

	const handleViewportChange = (viewport: any) => {
		setViewport({
			...viewport,
			// transitionDuration: 0,
		});
	};

	const handleMarkersUpdate = (map?: any) => {
		if (!mapRef.current && !map) {
			updateMarkers([]);
			return;
		}

		if (mapRef.current) {
			map = mapRef.current.getMap();
		}

		const features = map.querySourceFeatures(mvtSource.id, {
			sourceLayer: mvtSource.sourceLayer,
		});

		updateMarkers(features);
	};

	const handleInteractionStateChange = (s: ExtraState) => {
		if (s.isDragging || s.inTransition || s.isRotating || s.isZooming || s.isHovering || s.isPanning) {
		} else {
			handleMarkersUpdate();
		}
	};

	const onMapLoad = () => {
		console.log('onMapLoad');
		if (!mapRef.current) return;
		const map = mapRef.current.getMap();

		map.setLayoutProperty('country-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('settlement-major-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('settlement-minor-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('state-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('settlement-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('settlement-subdivision-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('poi-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('water-point-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('water-line-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('natural-point-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('natural-line-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('waterway-label', 'text-field', ['get', 'name_ru']);
		map.setLayoutProperty('road-label', 'text-field', ['get', 'name_ru']);

		const language = new MapboxLanguage();
		map.addControl(language);

		globalMap = map;

		handleMarkersUpdate();
	};

	const onMapClick = (event: any) => {
		console.log('onMapClick', event);
		if (mode === MapMods.addPoint) {
			lock = true;
			updateNewPoint(event);
			return;
		}

		const feature = event.features[0];
		const [longitude, latitude] = event.lngLat;

		// if feature is a cluster
		if (feature?.properties.count > 1) {
			setViewport({
				...viewport,
				longitude: feature.geometry.coordinates[0],
				latitude: feature.geometry.coordinates[1],
				zoom: feature.properties.expansionZoom,
				transitionDuration: 500,
			});

			history.push(`/`);
			selectPoint(null);
			return;
		}

		// clear selected point
		if (!feature?.id) {
			history.push(`/`);
			selectPoint(null);
			return;
		}
	};

	const onMapHover = () => {};

	const onMapMouseEnter = () => {
		mapRef.current.getMap().getCanvas().style.cursor = 'pointer';
	};

	const onMapMouseLeave = () => {
		mapRef.current.getMap().getCanvas().style.cursor = '';
	};

	const updateNewPoint = (event: any) => {
		setLocalNewPointLngLat(event.lngLat);
		setNewPointLngLat(event.lngLat);

		mapboxReverseGeocode(event.lngLat[0], event.lngLat[1]).then((res: any) => {
			if (!res?.features[0]) {
				return;
			}

			dadataSuggest(res?.features[0].place_name).then((sug) => {
				setDadataInput(sug.suggestions[0]);
			});
		});
	};

	const onDragMarker = (e: any) => {
		console.log(e);
	};

	return (
		<div className={styles.MapWrapper}>
			<MapGL
				{...viewport}
				width="100%"
				height="100%"
				mapStyle={mapBoxStyleStreets}
				mapboxApiAccessToken={mapBoxAccessToken}
				ref={mapRef}
				minZoom={4}
				maxZoom={21}
				interactiveLayerIds={mode === MapMods.showPoints ? ['clusters'] : undefined}
				onViewportChange={handleViewportChange}
				onLoad={onMapLoad}
				onClick={onMapClick}
				onMouseEnter={onMapMouseEnter}
				onMouseLeave={onMapMouseLeave}
				onHover={onMapHover}
				onInteractionStateChange={handleInteractionStateChange}
			>
				{mode === MapMods.showPoints && (
					<Source {...mvtSource} tiles={mvtSourceTiles} ref={sourceRef}>
						<Layer
							source={mvtSource.id}
							source-layer={mvtSource.sourceLayer}
							id="point-circle"
							type="circle"
							paint={{
								// 'circle-color': '#d3e7bf',
								'circle-color': 'red',
								'circle-radius': 5,
								'circle-stroke-width': 1,
								'circle-stroke-color': '#fff',
							}}
							// нужно чтобы не показываться точку, но получать
							filter={['has', 'some_undefined_data']}
							// filter={['==', ['get', 'count'], 1]}
						/>
						<Layer
							source={mvtSource.id}
							source-layer={mvtSource.sourceLayer}
							id="clusters"
							type="circle"
							filter={['>', ['get', 'count'], 1]}
							paint={{
								'circle-color': '#68a558',
								'circle-stroke-color': 'white',
								'circle-stroke-width': 1,
								'circle-opacity': 0.8,
								'circle-radius': [
									'step',
									['get', 'count'],
									12,
									5,
									13,
									7,
									14,
									9,
									15,
									10,
									16,
									15,
									17,
									20,
									18,
									30,
									19,
									40,
									20,
									50,
									21,
									75,
									22,
									100,
									23,
									150,
									24,
									200,
									25,
									300,
									26,
									400,
									27,
									500,
									28,
									600,
									29,
									750,
									30,
								],
							}}
						/>

						<Layer
							id="cluster_count"
							type="symbol"
							source={mvtSource.id}
							source-layer={mvtSource.sourceLayer}
							filter={['>', ['get', 'count'], 1]}
							layout={{
								'text-field': ['get', 'count'],
								'text-size': 12,
							}}
							paint={{
								'text-color': '#ffffff',
							}}
						/>
					</Source>
				)}

				{mode === MapMods.showPoints &&
					markersOnScreen.map((marker) => {
						return (
							<Marker
								longitude={marker.longitude}
								latitude={marker.latitude}
								key={marker.id}
								className={styles.pieMark}
							>
								<Tooltip title={marker.properties.title}>
									<div
										dangerouslySetInnerHTML={{
											__html: marker.el.innerHTML,
										}}
										onClick={(e) => handleMarkerClick(e, marker)}
									></div>
								</Tooltip>
							</Marker>
						);
					})}

				{selectedPoint && (
					<Marker
						longitude={selectedPoint.coordinates.coordinates[0]}
						latitude={selectedPoint.coordinates.coordinates[1]}
						className={styles.selectedMarker}
					>
						<div className={styles.selectedMarkIconWrapper}>
							<img className={styles.selectedMarkIcon} src={selectedPointIcon} />
						</div>
						<div className={styles.markText}>{selectedPoint.title}</div>
					</Marker>
				)}

				{localNewPointLngLat && (
					<Marker
						longitude={localNewPointLngLat[0]}
						latitude={localNewPointLngLat[1]}
						draggable
						className={styles.newPointMark}
						onDragEnd={updateNewPoint}
						onDrag={onDragMarker}
					>
						<img className={styles.newPointMarkIcon} src={newPointImg} />
					</Marker>
				)}

				<GeolocateControl
					className={styles.geolocateStyle}
					positionOptions={{ enableHighAccuracy: true }}
					trackUserLocation={true}
					showUserLocation={true}
				/>
				<div style={{ position: 'absolute', right: '10px', bottom: '350px' }}>
					<NavigationControl />
				</div>
			</MapGL>
		</div>
	);
});

export default MvtMap;
