import { mvtSourceUrl } from '../../constants/constants';

export const mvtSource = {
	id: 'points',
	type: 'vector',
	// tiles: [mvtSourceUrl],
	sourceLayer: 'points',
};
