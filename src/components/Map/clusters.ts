import { Layer } from 'react-map-gl';
import React from 'react';

export const layerPoints = {
	id: 'point',
	type: 'symbol',
	layout: {
		'icon-image': 'markerIcon',
		'icon-size': 0.25,
		'icon-allow-overlap': true,
	},
	paint: {
		'icon-color': '#007cbf',
		'icon-halo-color': '#007cbf',
		'text-color': '#007cbf',
	},
	filter: ['!', ['has', 'point_count']],
};

export const layerClusterCircle = {
	id: 'point-cluster-circle',
	type: 'circle',
	paint: {
		// 'circle-color': ['step', ['get', 'point_count'], '#68a558', 100, '#68a558', 750, '#68a558'],
		'circle-color': '#68a558',
		'circle-radius': ['step', ['get', 'point_count'], 15, 100, 20, 750, 25],
		// 'circle-radius': 15,
	},
	filter: ['has', 'point_count'],
};

export const layerClusterCount = {
	id: 'point-cluster-count',
	type: 'symbol',
	layout: {
		'text-field': '{point_count_abbreviated}',
		'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
		'text-size': 12,
	},
	paint: {},
	filter: ['has', 'point_count'],
};

export const layerPointCircle = {
	id: 'point-circle',
	type: 'circle',
	paint: {
		// 'circle-color': ['step', ['get', 'point_count'], '#68a558', 100, '#68a558', 750, '#68a558'],
		'circle-color': '#d3e7bf',
		// 'circle-radius': ['step', ['get', 'point_count'], 15, 100, 20, 750, 25],
		'circle-radius': 20,
		'circle-stroke-width': 1,
		'circle-stroke-color': '#fff',
	},
	filter: ['!', ['has', 'point_count']],
};
