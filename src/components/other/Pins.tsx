// import React, { PureComponent } from 'react';
// import { Marker } from 'react-map-gl';
// import markerIcon from '../../icons/recycle-transparent.png';
//
// export default class Pins extends PureComponent<any> {
// 	render() {
// 		const { data, onClick } = this.props;
//
// 		if (!data) {
// 			return null;
// 		}
//
// 		return data.map((point: any, index: any) => (
// 			<Marker key={`marker-${index}`} longitude={point.Longitude} latitude={point.Latitude}>
// 				<button
// 					onClick={(e) => {
// 						e.preventDefault();
// 						onClick(point);
// 					}}
// 				>
// 					<img src={markerIcon} alt="Point mark" width="10px" height="10px" />
// 				</button>
// 			</Marker>
// 		));
// 	}
// }
