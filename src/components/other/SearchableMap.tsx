// import 'mapbox-gl/dist/mapbox-gl.css';
// import 'react-map-gl-geocoder/dist/mapbox-gl-geocoder.css';
// import React, { Component } from 'react';
// import MapGL, { GeolocateControl, Layer, Source } from 'react-map-gl';
// // @ts-ignore
// import DeckGL, { GeoJsonLayer } from 'deck.gl';
// // @ts-ignore
// import Geocoder from 'react-map-gl-geocoder';
// import { mapBoxAccessToken, mapBoxStyleLight } from '../../constants/constants';
// import { api } from '../../api';
//
// const token = mapBoxAccessToken;
//
// import '../Map/Map.scss';
// import { IPoint } from '../../interfaces/IPoint';
//
// interface State {
// 	viewport: any;
// 	searchResultLayer: any;
// 	geojson: any;
// 	loading: boolean;
// 	points: IPoint[];
// }
// class SearchableMap extends Component {
// 	state: State = {
// 		viewport: {
// 			width: '100%',
// 			height: '100%',
// 			latitude: 55.749489,
// 			longitude: 37.619649,
// 			zoom: 10,
// 		},
// 		searchResultLayer: null,
// 		geojson: {
// 			type: 'FeatureCollection',
// 			features: [
// 				// {type: 'Feature', geometry: {type: 'Point', coordinates: [-122.4, 37.8]}},
// 				// {type: 'Feature', geometry: {type: 'Point', coordinates: [37.4, 37.8]}}
// 			],
// 		},
// 		loading: true,
// 		points: null,
// 	};
//
// 	mapRef = React.createRef<any>();
//
// 	handleViewportChange = (viewport: any) => {
// 		this.setState({
// 			viewport: { ...this.state.viewport, ...viewport },
// 		});
// 	};
// 	// if you are happy with Geocoder default settings, you can just use handleViewportChange directly
// 	handleGeocoderViewportChange = (viewport: any) => {
// 		const geocoderDefaultOverrides = { transitionDuration: 1000 };
//
// 		return this.handleViewportChange({
// 			...viewport,
// 			...geocoderDefaultOverrides,
// 		});
// 	};
//
// 	handleOnResult = (event: { result: { geometry: any } }) => {
// 		this.setState({
// 			searchResultLayer: new GeoJsonLayer({
// 				id: 'search-result',
// 				data: event.result.geometry,
// 				getFillColor: [255, 0, 0, 128],
// 				getRadius: 1000,
// 				pointRadiusMinPixels: 10,
// 				pointRadiusMaxPixels: 10,
// 			}),
// 		});
// 	};
//
// 	componentDidMount() {
// 		api.getPoints().then((points) => {
// 			// alert(JSON.stringify(points));
//
// 			const features: any[] = [];
// 			if (Array.isArray(points)) {
// 				points.forEach((p) => {
// 					features.push({
// 						type: 'Feature',
// 						geometry: {
// 							type: 'Point',
// 							coordinates: [p.Longitude, p.Latitude],
// 						},
// 						state: p,
// 						id: p.Title,
// 					});
// 				});
// 			}
//
// 			this.setState({
// 				geojson: { ...this.state.geojson, features },
// 				loading: false,
// 				points,
// 			});
// 		});
// 	}
//
// 	_onClick = (e: any) => {
// 		console.log(e);
// 		if (this.state.points) {
// 			const p = this.state.points.find(({ Title }) => Number(Title) === e.features[0].id);
// 			alert(JSON.stringify(p));
// 		}
// 	};
//
// 	render() {
// 		const { viewport, searchResultLayer } = this.state;
// 		return (
// 			<div style={{ height: '100%' }}>
// 				{this.state.loading ? (
// 					<div></div>
// 				) : (
// 					<>
// 						<MapGL
// 							ref={this.mapRef}
// 							{...viewport}
// 							mapStyle={mapBoxStyleLight}
// 							width="100%"
// 							height="100%"
// 							onViewportChange={this.handleViewportChange}
// 							mapboxApiAccessToken={token}
// 							onClick={this._onClick}
// 						>
// 							<GeolocateControl
// 								className={'geolocateStyle'}
// 								positionOptions={{ enableHighAccuracy: true }}
// 								trackUserLocation={true}
// 							/>
//
// 							<Geocoder
// 								mapRef={this.mapRef}
// 								onResult={this.handleOnResult}
// 								onViewportChange={this.handleGeocoderViewportChange}
// 								mapboxApiAccessToken={token}
// 								position="top-left"
// 							/>
//
// 							<Source id="my-data" type="geojson" data={this.state.geojson} cluster>
// 								<Layer
// 									id="point"
// 									type="circle"
// 									paint={{
// 										'circle-radius': 10,
// 										'circle-color': '#007cbf',
// 									}}
// 								/>
// 							</Source>
// 						</MapGL>
// 						{/*<DeckGL {...viewport} layers={[searchResultLayer]} />*/}
// 					</>
// 				)}
// 			</div>
// 		);
// 	}
// }
//
// export default SearchableMap;
