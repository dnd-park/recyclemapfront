import React, { useEffect, useState } from 'react';
import MapGL, {
	GeolocateControl,
	Layer,
	Marker,
	Popup,
	Source,
	ViewportProps,
	NavigationControl,
	ExtraState,
} from 'react-map-gl';
import {
	fractionIds,
	fractionsDefault,
	mapBoxAccessToken,
	mapBoxStyleLight,
	mapBoxStyleStreets,
	pointsStyle,
} from '../../constants/constants';

import markerIcon from '../../icons/recycle-symbol-green-128.png';
import { IPoint } from '../../interfaces/IPoint';

import styles from '../Map/Map.scss';
import { Feature } from 'geojson';
import { scaleLinear, min, max, select, arc, pie, ascending } from 'd3';

const RecycleMapUrbica: React.FC = () => {
	const [viewport, setViewport] = useState<any>({
		width: '100%',
		height: '100%',
		// latitude: 59.662387,
		// longitude: 12.734936,
		latitude: 55.749489,
		longitude: 37.619649,
		zoom: 12,
	});
	const [points, setPoints] = useState<IPoint[]>(null);
	const [pointsGeo, setPointsGeo] = useState(null);
	const [selectedPoint, setSelectedPoint] = useState<IPoint>(null);

	const mapRef = React.createRef<any>();
	const sourceRef = React.createRef<any>();

	useEffect(() => {
		const listener = (e: any) => {
			if (e.key === 'Escape') {
				setSelectedPoint(null);
			}
		};
		window.addEventListener('keydown', listener);

		return () => {
			window.removeEventListener('keydown', listener);
		};
	}, []);

	const onMapClick = (e: any) => {
		console.log(e);
		const feature = e.features[0];
		const [longitude, latitude] = e.lngLat;
	};

	const onMapMouseEnter = () => {
		mapRef.current.getMap().getCanvas().style.cursor = 'pointer';
	};

	const onMapMouseLeave = () => {
		mapRef.current.getMap().getCanvas().style.cursor = '';
	};

	let markers: any = {};
	let markersOnScreen: any = {};
	let point_counts: number[] = [];
	let totals: number;

	const [visibleMarkers, setVisibleMarkers] = useState<any>([]);

	const updateMarkers = () => {
		if (!mapRef.current) {
			return;
		}

		const map = mapRef.current.getMap();
		// const features2 = map.querySourceFeatures('points', {
		// 	sourceLayer: 'recycle_points',
		// });
		const features2 = map.querySourceFeatures('points', {
			sourceLayer: 'points',
		});
		const clusterFeatures = features2.filter((f: any) => {
			return f.properties.count;
		});
		console.log('features amount', features2.length);
		console.log('clusterFeatures amount', clusterFeatures.length);
		console.log('clusterFeature', clusterFeatures[0]);
		console.log('features2', features2[0]);

		// const features = features2.slice(0, 10);
		const features = features2;

		totals = features.length;

		let newMarkers: any = {};

		features.forEach((feature: Feature<any>) => {
			const coordinates = feature.geometry.coordinates;
			const props = feature.properties;
			const id: number = props.id;
			// const id: string = props.lat + '' + props.lng;

			if (props.count > 1) {
				return;
			}

			let marker: any = markers[id];
			if (!marker) {
				const el = createDonutChart(props, [0, features.length]);

				// marker = markers[id] = new mapboxgl.Marker({
				// 	element: el,
				// }).setLngLat(coordinates);
				marker = markers[id] = {
					id,
					el,
					longitude: coordinates[0],
					latitude: coordinates[1],
				};
			}

			newMarkers[id] = marker;

			// if (!markersOnScreen[id]) {
			// 	marker.addTo(map);
			// }
		});

		console.log('newMarkers amount', Object.keys(newMarkers).length);

		// for (let id in markersOnScreen) {
		// 	if (!newMarkers[id]) {
		// 		markersOnScreen[id].remove();
		// 	}
		// }

		markersOnScreen = newMarkers;

		setVisibleMarkers(
			Object.keys(markersOnScreen).map((key) => {
				return markersOnScreen[key];
			}),
		);

		console.log('markersOnScreen length', Object.keys(markersOnScreen).length);
	};

	const getPointCount = (features: any[]) => {
		// features.forEach((f) => {
		// 	if (f.properties.cluster) {
		// 		point_counts.push(f.properties.point_count);
		// 	}
		// });
		// return point_counts;
	};

	const colorScale = (type: string) => {
		return fractionsDefault.find((f) => f.fraction_value === type).color;
	};

	const createDonutChart = (props: any, totals: number[]) => {
		const div = document.createElement('div');

		const fracs = props.fractions ? props.fractions.split(',').map((id: string) => Number(id)) : [];

		const singleFraction = fracs.length <= 1;

		const data = [
			{
				type: 'appliances',
				// count: props.appliances ? 1 : 0,
				count: fracs.includes(fractionIds.appliances) ? 1 : 0,
			},
			{
				type: 'batteries',
				// count: props.batteries ? 1 : 0,
				count: fracs.includes(fractionIds.batteries) ? 1 : 0,
			},
			{
				type: 'clothes',
				// count: props.clothes ? 1 : 0,
				count: fracs.includes(fractionIds.clothes) ? 1 : 0,
			},
			{
				type: 'glass',
				// count: props.glass ? 1 : 0,
				count: fracs.includes(fractionIds.glass) ? 1 : 0,
			},
			{
				type: 'light_bulbs',
				// count: props.light_bulbs ? 1 : 0,
				count: fracs.includes(fractionIds.light_bulbs) ? 1 : 0,
			},
			{
				type: 'caps',
				// count: props.caps ? 1 : 0,
				count: fracs.includes(fractionIds.caps) ? 1 : 0,
			},
			{
				type: 'metal',
				// count: props.metal ? 1 : 0,
				count: fracs.includes(fractionIds.metal) ? 1 : 0,
			},

			{
				type: 'other',
				// count: props.other ? 1 : 0,
				count: fracs.includes(fractionIds.other) ? 1 : 0,
			},
			{
				type: 'paper',
				// count: props.paper ? 1 : 0,
				count: fracs.includes(fractionIds.paper) ? 1 : 0,
			},
			{
				type: 'plastic',
				// count: props.plastic ? 1 : 0,
				count: fracs.includes(fractionIds.plastic) ? 1 : 0,
			},
			{
				type: 'tetra_pack',
				// count: props.tetra_pack ? 1 : 0,
				count: fracs.includes(fractionIds.tetra_pack) ? 1 : 0,
			},
			{
				type: 'tires',
				// count: props.tires ? 1 : 0,
				count: fracs.includes(fractionIds.tires) ? 1 : 0,
			},
			{
				type: 'toxic',
				// count: props.toxic ? 1 : 0,
				count: fracs.includes(fractionIds.toxic) ? 1 : 0,
			},
		];

		const thickness = singleFraction ? 2 : 4;

		// const scale = scaleLinear()
		// 	.domain([min(totals), max(totals)])
		// 	.range([500, max(totals)]);

		// const radius = Math.sqrt(scale(1));
		const radius = 10;
		const circleRadius = radius - thickness;

		const svg = select(div)
			.append('svg')
			.attr('class', 'pie')
			.attr('width', radius * 3)
			.attr('height', radius * 3);

		const g = svg.append('g').attr('transform', `translate(${radius * 1.5}, ${radius * 1.5})`);

		const circle = g
			.append('circle')
			.attr('r', radius + 2)
			.attr('fill', 'rgba(255,255,255,0.7)')
			.attr('class', 'center-circle');

		const myArc = arc()
			.innerRadius(radius - thickness)
			.outerRadius(radius);

		const myPie = pie()
			// @ts-ignore
			.value((d) => d.count)
			.sort(null);

		const path = g
			.selectAll('path')
			// @ts-ignore
			.data(myPie(data.sort((x, y) => ascending(y.count, x.count))))
			.enter()
			.append('path')
			// @ts-ignore
			.attr('d', myArc)
			// @ts-ignore
			.attr('fill', (d) => colorScale(d.data.type));

		if (!singleFraction) {
			path.attr('stroke', 'white');
		}

		// create the text
		const text = g
			.append('text')
			.attr('class', styles.pieMarkText)
			.text(props.count)
			.attr('text-anchor', 'middle')
			.attr('dy', 4)
			.attr('fill', '#00000087');

		return div;
	};

	const handleViewportChange = (viewport: any, s: ExtraState) => {
		// if (s.isDragging || s.inTransition || s.isRotating || s.isZooming || s.isHovering || s.isPanning) {
		// } else {
		// 	updateMarkers();
		// }

		setViewport(viewport);
	};

	const handleInteractionStateChange = (s: ExtraState) => {
		// if (s.isDragging || s.inTransition || s.isRotating || s.isZooming || s.isHovering || s.isPanning) {
		// } else {
		updateMarkers();
		// }
	};

	return (
		<MapGL
			{...viewport}
			mapStyle={mapBoxStyleStreets}
			mapboxApiAccessToken={mapBoxAccessToken}
			ref={mapRef}
			onViewportChange={handleViewportChange}
			onLoad={() => {
				if (!mapRef.current) return;
				console.log('on map load ', mapRef.current);
				const map = mapRef.current.getMap();
				map.loadImage(markerIcon, (error: any, image: any) => {
					if (error) return;
					map.addImage('markerIcon', image);
				});
				updateMarkers();
			}}
			onClick={onMapClick}
			// interactiveLayerIds={[layerPoints.id, layerClusterCircle.id]}
			onMouseEnter={onMapMouseEnter}
			onMouseLeave={onMapMouseLeave}
			onInteractionStateChange={handleInteractionStateChange}
			minZoom={4}
			maxZoom={21}
		>
			<div style={{ position: 'absolute', right: '10px', bottom: '100px' }}>
				<NavigationControl />
			</div>

			{/*<Source id="points" type="vector" tiles={['https://recyclemap.ru/vtiles/1/{z}/{x}/{y}.pbf']} tolerance={0}>*/}
			{/*	<Layer*/}
			{/*		source="vtiles"*/}
			{/*		source-layer="recycle_points"*/}
			{/*		id="point-circle"*/}
			{/*		type="circle"*/}
			{/*		paint={{*/}
			{/*			'circle-color': '#d3e7bf',*/}
			{/*			'circle-radius': 20,*/}
			{/*			'circle-stroke-width': 1,*/}
			{/*			'circle-stroke-color': '#fff',*/}
			{/*		}}*/}
			{/*		// нужно чтобы не показываться точку, но получать*/}
			{/*		filter={['has', 'some_undefine_data']}*/}
			{/*	/>*/}
			{/*</Source>*/}

			{/*<Source id="points" type="vector" tiles={['http://localhost:3005/points/{z}/{x}/{y}/tile.mvt']}>*/}
			{/*	<Layer*/}
			{/*		source="points"*/}
			{/*		source-layer="points"*/}
			{/*		id="point-circle"*/}
			{/*		type="circle"*/}
			{/*		paint={{*/}
			{/*			'circle-color': '#d3e7bf',*/}
			{/*			'circle-radius': 20,*/}
			{/*			'circle-stroke-width': 1,*/}
			{/*			'circle-stroke-color': '#fff',*/}
			{/*		}}*/}
			{/*		// нужно чтобы не показываться точку, но получать*/}
			{/*		filter={['has', 'some_undefine_data']}*/}
			{/*	/>*/}
			{/*</Source>*/}

			<Source
				id="points"
				type="vector"
				tiles={[
					// 'http://localhost:3005/points/{z}/{x}/{y}/tile.mvt',
					'https://vm417203.eurodir.ru/mvtapi/points/{z}/{x}/{y}/tile.mvt.pbf',
					// 'http://localhost:5000/points/{z}/{x}/{y}/tile.mvt.pbf',
					// 'https://floating-reef-32095.herokuapp.com/points/{z}/{x}/{y}/tile.mvt.pbf',
				]}
			>
				<Layer
					source="points"
					source-layer="points"
					id="point-circle"
					type="circle"
					paint={{
						'circle-color': '#d3e7bf',
						'circle-radius': 10,
						'circle-stroke-width': 1,
						'circle-stroke-color': '#fff',
					}}
					// нужно чтобы не показываться точку, но получать
					filter={['has', 'some_undefine_data']}
					// filter={['==', ['get', 'count'], 1]}
				/>

				<Layer
					source="points"
					source-layer="points"
					id="clusters"
					type="circle"
					filter={['>', ['get', 'count'], 1]}
					paint={{
						'circle-color': '#68a558',
						'circle-radius': [
							'step',
							['get', 'count'],
							12,
							5,
							13,
							7,
							14,
							9,
							15,
							10,
							16,
							15,
							17,
							20,
							18,
							30,
							19,
							40,
							20,
							50,
							21,
							75,
							22,
							100,
							23,
							150,
							24,
							200,
							25,
							300,
							26,
							400,
							27,
							500,
							28,
							600,
							29,
							750,
							30,
						],
					}}
				/>
				<Layer
					id="cluster_count"
					type="symbol"
					source="points"
					source-layer="points"
					filter={['>', ['get', 'count'], 1]}
					layout={{
						'text-field': ['get', 'count'],
						'text-size': 12,
					}}
					paint={{
						'text-color': '#ffffff',
					}}
				/>

				{/*<Layer*/}
				{/*	id="unclustered-point"*/}
				{/*	type="circle"*/}
				{/*	source="points"*/}
				{/*	source-layer="points"*/}
				{/*	filter={['==', ['get', 'count'], 1]}*/}
				{/*	paint={{*/}
				{/*		'circle-color': '#11b4da',*/}
				{/*		'circle-radius': 4,*/}
				{/*		'circle-stroke-width': 1,*/}
				{/*		'circle-stroke-color': '#fff',*/}
				{/*	}}*/}
				{/*/>*/}
			</Source>

			{visibleMarkers.map((marker: any) => {
				return (
					<Marker
						longitude={marker.longitude}
						latitude={marker.latitude}
						key={marker.id}
						className={styles.pieMark}
					>
						<div
							dangerouslySetInnerHTML={{
								__html: marker.el.innerHTML,
							}}
						></div>
					</Marker>
				);
			})}
		</MapGL>
	);
};

export default RecycleMapUrbica;
