// import React, {useEffect, useState} from "react";
// // import mapboxGl, {Map as MBMap} from 'mapbox-gl';
// import {mapBoxAccessToken, mapBoxStyleLight} from '../../constants/constants';
// import '../Map/Map.scss';
// import {api} from '../../api';
// import ReactMapGL, {GeolocateControl, Layer, Source, ViewportProps} from 'react-map-gl';
//
// // import 'mapbox-gl/dist/mapbox-gl.css'
//
// import {SVGOverlay} from 'react-map-gl';
//
// function redraw({project}: any) {
// 	const [cx, cy] = project([-122, 37]);
// 	return <circle cx={cx} cy={cy} r={4} fill="blue" />;
// }
//
// const mapStyle = {
// 	version: 8,
// 	sources: {
// 		points: {
// 			type: 'geojson',
// 			data: {
// 				type: 'FeatureCollection',
// 				features: [
// 					{type: 'Feature', geometry: {type: 'Point', coordinates: [-122.45, 37.78]}}
// 				]
// 			}
// 		}
// 	},
// 	layers: [
// 		{
// 			id: 'my-layer',
// 			type: 'circle',
// 			source: 'points',
// 			paint: {
// 				'circle-color': '#f00',
// 				'circle-radius': 4
// 			}
// 		}
// 	]
// };
//
// interface Viewport {
// 	viewport: {
// 		width: number | string;
// 		height: number | string;
// 		latitude: number;
// 		longitude: number;
// 		zoom: number;
// 	}
// }
//
// const MapOld: React.FC = () => {
// 	// let map: MBMap;
// 	const [state, setState] = useState<Viewport>({
// 		viewport: {
// 			width: "100%",
// 			height: "100%",
// 			latitude: 55.749489,
// 			longitude: 37.619649,
// 			zoom: 10
// 		}
// 	});
//
// 	const _onViewportChange = (viewport: ViewportProps) => setState({viewport: {
// 		...viewport,
// 			// transitionDuration: 3000
// 	}})
//
//
// 	useEffect(() => {
// 		// mapboxGl.accessToken = mapBoxAccessToken;
// 		// map = new MBMap({
// 		// 	container: 'mapbox-root',
// 		// 	style: mapBoxStyle,
// 		// 	center: [37.619649, 55.749489],
// 		// 	zoom: 10,
// 		// });
// 		// map.on('load', () => {
// 		// 	// map.addLayer({});
// 		// });
// 		// map.on('click', (e) => {
// 		// 	new mapboxGl.Popup()
// 		// 		.setLngLat(e.lngLat)
// 		// 		.setHTML(e.lngLat as any)
// 		// 		.addTo(map);
// 		// });
// 		// map.on('mouseenter', () => {
// 		// 	map.getCanvas().style.cursor = 'pointer';
// 		// });
// 		// map.on('mouseleave', () => {
// 		// 	map.getCanvas().style.cursor = '';
// 		// });
// 		// map.addControl(new mapboxGl.NavigationControl());
// 		// map.addControl(new mapboxGl.ScaleControl());
// 		// map.addControl(new mapboxGl.FullscreenControl());
//
//
// 		api.getPoints().then(points => {
// 			// alert(JSON.stringify(points));
// 		});
//
// 	}, []);
//
// 	const geojson: any = {
// 		type: 'FeatureCollection',
// 		features: [
// 			{type: 'Feature', geometry: {type: 'Point', coordinates: [-122.4, 37.8]}},
// 			{type: 'Feature', geometry: {type: 'Point', coordinates: [37.4, 37.8]}}
// 		]
// 	};
//
// 	return (
// 		// <div id="mapbox-root"></div>
// 		<ReactMapGL
// 			{...state.viewport}
// 			mapStyle={mapBoxStyleLight}
// 			// mapStyle={mapStyle}
// 			onViewportChange={_onViewportChange}
// 			mapboxApiAccessToken={mapBoxAccessToken}
// 		>
// 			<SVGOverlay redraw={redraw} />
//
// 			<GeolocateControl
// 				className={'geolocateStyle'}
// 				positionOptions={{enableHighAccuracy: true}}
// 				trackUserLocation={true}
// 			/>
//
// 			<Source id="my-data" type="geojson" data={geojson}>
// 				<Layer
// 					id="point"
// 					type="circle"
// 					paint={{
// 						'circle-radius': 10,
// 						'circle-color': '#007cbf'
// 					}} />
// 			</Source>
// 		</ReactMapGL>
// 	);
// };
//
// export default MapOld;
