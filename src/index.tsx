import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ConfigProvider } from 'antd';
import ruRU from 'antd/lib/locale/ru_RU';

import App from './components/App/App';
import RecyclePointStore from './modules/RecyclePointStore';
import UserStore from './modules/UserStore';
import UtilStore from './modules/UtilStore';

UserStore.getCurrentUser(true);
UserStore.getFavoritePoints();
// RecyclePointStore.getAll();
UtilStore.loadCities();
UtilStore.loadCities();

ReactDOM.render(
	<BrowserRouter>
		<ConfigProvider locale={ruRU}>
			<App />
		</ConfigProvider>
	</BrowserRouter>,
	document.getElementById('root'),
);
