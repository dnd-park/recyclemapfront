export enum ActionTypes {
	published = 'published',
	unpublished = 'unpublished',
	approved = 'approved',
	denied = 'denied',
	created = 'created',
}

export enum ActionItemTypes {
	request = 'request',
	point = 'point',
}

export interface IActionActor {
	id: number;
	name: string;
}

export interface IActionItem {
	id: number;
	type: ActionItemTypes;
	name: string;
}

export interface IAction {
	action: ActionTypes;
	actor: IActionActor;
	city: number;
	createdAt: string;
	id: number;
	item: IActionItem;
	title: string;
}

export interface IActionsResponse {
	actions: IAction[];
}

export interface IActionRequestParams {
	actors?: number[];
	types?: ActionTypes;
	limit?: number;
	page?: number;
	desc?: boolean;
}
