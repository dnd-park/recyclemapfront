import { IProperties } from './IProperties';

export interface IMarker {
	id: number;
	el: HTMLElement;
	longitude: number;
	latitude: number;
	properties: IProperties;
}

export interface IMarkers {
	[key: number]: IMarker;
}
