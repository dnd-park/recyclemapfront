export interface IFraction {
	id: number;
	name: string;
	icon: any;
	selected: boolean;
	color: string;
	fraction_value?: string;
	fraction_order?: number;
}
