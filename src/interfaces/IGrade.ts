export interface IGrade {
	pointID: number;
	grade: number;
}

export interface IGradesRequestData {
	grades: IGrade[];
}
