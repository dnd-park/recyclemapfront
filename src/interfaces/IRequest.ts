import { ICompany, ITimetable } from './IPoint';

export enum RequestStatus {
	ON_CHECK = 'ON_CHECK',
	APPROVED = 'APPROVED',
	DENIED = 'DENIED',
}

export const RequestStatusNames = {
	[RequestStatus.ON_CHECK]: 'На проверке',
	[RequestStatus.APPROVED]: 'Принята',
	[RequestStatus.DENIED]: 'Отклонена',
};

export interface IRequest {
	address: string;
	group: ICompany;
	description: string;
	grades: number;
	id: number;
	images: string[];
	latitude: number;
	longitude: number;
	rubbishType: number[];
	title: string;
	updated: string;
	coordinates: any;
	fractions: number[];
	timetable?: ITimetable;
	createdBy?: number;
	phones?: string[];
	emails?: string[];
	sites?: string[];
	available?: boolean;
	status?: RequestStatus;
}
