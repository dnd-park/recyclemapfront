export interface IComment {
	comment: string;
	id?: number;
	point_id?: number;
	updated?: string;
	author?: {
		id: number;
		firstName: string;
		secondName: string;
	};
}
