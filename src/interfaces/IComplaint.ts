export interface IComplaint {
	description: string;
	email: string;
	point_id?: number;
}
