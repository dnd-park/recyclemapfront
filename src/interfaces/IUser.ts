export interface ILoginData {
	login: string;
	password: string;
}

export interface ISignupData {
	login: string;
	password: string;
	first_name: string;
	second_name: string;
}

export interface IUserEditData {
	first_name: string;
	second_name: string;
}

export enum UserTypes {
	user = 'user',
	moderator = 'moderator',
	admin = 'admin',
}

export enum UserStatuses {
	empty = '',
	active = 'active',
}

export interface IUser {
	login: string;
	first_name: string;
	second_name: string;
	city: number;
	last_updated: string;
	phones: string[] | null;
	registration_date: string;
	status: UserStatuses;
	type: UserTypes;
	avatar: string;
}
