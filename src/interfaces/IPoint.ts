export interface ICompany {
	ID: number;
	Name: string;
	Phone: string;
	Address: string;
}

export enum Weekdays {
	monday = 'monday',
	tuesday = 'tuesday',
	wednesday = 'wednesday',
	thursday = 'thursday',
	friday = 'friday',
	saturday = 'saturday',
	sunday = 'sunday',
}

export interface ITimetableWeekday {
	name: Weekdays;
	startTime: string;
	endTime: string;
	work: boolean;
}

export interface ITimetable {
	isFloating: boolean;
	weekdays: ITimetableWeekday[];
	breakStartTime: string;
	breakEndTime: string;
	campaignStartTime: string;
	campaignEndTime: string;
	isRoundTheClock?: boolean;
	isBreak?: boolean;
}

export interface ICoordinates {
	type: 'Point';
	coordinates: [number, number];
}

export enum PointStaus {
	FROZEN = 'FROZEN',
	PUBLISHED = 'PUBLISHED',
}

export const PointStatusNames = {
	[PointStaus.FROZEN]: 'Скрыто',
	[PointStaus.PUBLISHED]: 'Опубликовано',
};

export interface IPoint {
	grade: number;
	address: string;
	group: ICompany;
	description: string;
	grades: number;
	id: number;
	images: string[];
	latitude: number;
	longitude: number;
	rubbishType: number[];
	title: string;
	updated: string;
	coordinates: ICoordinates;
	fractions: number[];
	timetable?: ITimetable;
	createdBy?: number;
	phones?: string[];
	emails?: string[];
	sites?: string[];
	available?: boolean;
	timestring?: string;
	status?: PointStaus;
}
