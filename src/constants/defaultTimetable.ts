import { ITimetable, Weekdays } from '../interfaces/IPoint';

export const defaultTimetable: ITimetable = {
	isFloating: false,
	weekdays: [
		{
			name: Weekdays.monday,
			startTime: '9:00AM',
			endTime: '9:00PM',
			work: true,
		},
		{
			name: Weekdays.tuesday,
			startTime: '9:00AM',
			endTime: '9:00PM',
			work: true,
		},
		{
			name: Weekdays.wednesday,
			startTime: '9:00AM',
			endTime: '9:00PM',
			work: true,
		},
		{
			name: Weekdays.thursday,
			startTime: '9:00AM',
			endTime: '9:00PM',
			work: true,
		},
		{
			name: Weekdays.friday,
			startTime: '9:00AM',
			endTime: '9:00PM',
			work: true,
		},
		{
			name: Weekdays.saturday,
			startTime: '9:00AM',
			endTime: '9:00PM',
			work: false,
		},
		{
			name: Weekdays.sunday,
			startTime: '9:00AM',
			endTime: '9:00PM',
			work: false,
		},
	],
	breakStartTime: '4:30PM',
	breakEndTime: '5:30PM',
	campaignStartTime: 'Monday, 21-Jan-20 15:04:05 MST',
	campaignEndTime: 'Monday, 04-Jan-21 15:04:05 MST',
};
