import { ICity } from '../modules/UtilStore';

export const citiesArr: ICity[] = [
	{
		name: 'Москва',
		id: 0,
	},
	{
		name: 'Санкт-Петербург',
		id: 1,
	},
	{
		name: 'Нижний Новгород',
		id: 2,
	},
	{
		name: 'Екатеринбург',
		id: 3,
	},
	{
		name: 'Казань',
		id: 4,
	},
	{
		name: 'Волгоград',
		id: 5,
	},
	{
		name: 'Пермь',
		id: 6,
	},
];
