import { IFraction } from '../interfaces/IFraction';

export const mapBoxAccessToken = process.env.MAPBOX_TOKEN;
export const mapBoxStyleStreets = 'mapbox://styles/mapbox/streets-v11';
export const mapBoxStyleLight = 'mapbox://styles/mapbox/light-v10';
export const mapBoxStyleDark = 'mapbox://styles/mapbox/dark-v10';
export const pointsStyle = 'mapbox://styles/nozim/ckgebx8sz2jwy19pe5xvkkz13?optimize=true';
export const dadataApiKey = process.env.DADATA_API_KEY;
export const dadataApiSecretKey = process.env.DADATA_API_SECRET_KEY;

// export const apiBaseUrl = 'https://localhost:8080/';
// export const apiBaseUrl = 'https://recycle-map-back.herokuapp.com';
export const apiBaseUrl = 'https://vm417203.eurodir.ru/api/';

// export const mvtSourceUrl = 'http://localhost:5000/points/{z}/{x}/{y}/tile.mvt.pbf';
export const mvtSourceUrl = 'https://vm417203.eurodir.ru/mvtapi/points/{z}/{x}/{y}/tile.mvt.pbf';

const recycleStaticBaseUrl = 'https://recyclemap.ru';

export const icons = {
	info: recycleStaticBaseUrl + '/static/media/info.0207a06d.svg',
	mark: recycleStaticBaseUrl + '/static/media/point.b6e09290.svg',
};

import plastic from './../icons/fractions/plastic.svg';
import glass from './../icons/fractions/glass.svg';
import paper from './../icons/fractions/paper.svg';
import metal from './../icons/fractions/metal.svg';
import tetra_pack from './../icons/fractions/tetra_pack.svg';
import batteries from './../icons/fractions/batteries.svg';
import light_bulbs from './../icons/fractions/light_bulbs.svg';
import appliances from './../icons/fractions/appliances.svg';
import toxic from './../icons/fractions/toxic.svg';
import other from './../icons/fractions/other.svg';
import caps from './../icons/fractions/caps.svg';
import tires from './../icons/fractions/tires.svg';
import clothes from './../icons/fractions/clothes.svg';
import { Weekdays } from '../interfaces/IPoint';

export const fractionsDefault: IFraction[] = [
	{ id: 3, name: 'Пластик', fraction_value: 'plastic', selected: false, icon: plastic, color: '#f78224' },
	{
		id: 2,
		name: 'Стекло',
		fraction_value: 'glass',
		selected: false,
		icon: glass,
		// color: '#68ba51',
		color: '#69f49c',
	},
	{
		id: 1,
		name: 'Бумага',
		fraction_value: 'paper',
		fraction_order: 3,
		selected: false,
		icon: paper,
		color: '#568fcc',
	},
	{
		id: 4,
		name: 'Металл',
		fraction_value: 'metal',
		fraction_order: 4,
		selected: false,
		icon: metal,
		color: '#f96455',
	},
	{
		id: 11,
		name: 'Tetra Pak',
		fraction_value: 'tetra_pack',
		fraction_order: 5,
		selected: false,
		icon: tetra_pack,
		color: '#9cd7a2',
	},
	{
		id: 8,

		name: 'Батарейки',
		fraction_value: 'batteries',
		fraction_order: 6,
		selected: false,
		icon: batteries,
		color: '#c69745',
	},
	{
		id: 9,
		name: 'Лампочки',
		fraction_value: 'light_bulbs',
		fraction_order: 7,
		selected: false,
		icon: light_bulbs,
		color: '#984cff',
	},
	{
		id: 5,
		name: 'Одежда',
		fraction_value: 'clothes',
		fraction_order: 8,
		selected: false,
		icon: clothes,
		color: '#f259f1',
	},
	{
		id: 10,
		name: 'Бытовая техника',
		fraction_value: 'appliances',
		fraction_order: 9,
		selected: false,
		icon: appliances,
		color: '#ba4b48',
	},
	{
		id: 7,
		name: 'Опасные отходы',
		fraction_value: 'toxic',
		fraction_order: 10,
		selected: false,
		icon: toxic,
		// color: '#3a3939',
		color: '#e42e4f',
	},
	{
		id: 6,
		name: 'Иное',
		fraction_value: 'other',
		fraction_order: 11,
		selected: false,
		icon: other,
		color: '#74edf2',
	},
	{
		id: 12,
		name: 'Крышечки',
		fraction_value: 'caps',
		fraction_order: 12,
		selected: false,
		icon: caps,
		// color: '#e0d925',
		color: '#edee50',
	},
	{
		id: 13,
		name: 'Шины',
		fraction_value: 'tires',
		fraction_order: 13,
		selected: false,
		icon: tires,
		// color: '#3d0a08',
		color: '#aec5ec',
	},
];

export const enum fractionNames {
	plastic = 'plastic',
	glass = 'glass',
	paper = 'paper',
	metal = 'metal',
	tetra_pack = 'tetra_pack',
	batteries = 'batteries',
	light_bulbs = 'light_bulbs',
	clothes = 'clothes',
	appliances = 'appliances',
	toxic = 'toxic',
	caps = 'caps',
	tires = 'tires',
	other = 'other',
}

export const enum fractionIds {
	paper = 1,
	glass = 2,
	plastic = 3,
	metal = 4,
	tetra_pack = 5,
	batteries = 6,
	light_bulbs = 7,
	clothes = 8,
	appliances = 9,
	toxic = 10,
	caps = 11,
	tires = 12,
	other = 13,
}

export const fractionOptions = fractionsDefault.map((fraction) => {
	return {
		key: fraction.id,
		value: fraction.id,
		label: fraction.name,
	};
});

export const weekdaysRus = {
	[Weekdays.monday]: 'Понедельник',
	[Weekdays.tuesday]: 'Вторник',
	[Weekdays.wednesday]: 'Среда',
	[Weekdays.thursday]: 'Четверг',
	[Weekdays.friday]: 'Пятница',
	[Weekdays.saturday]: 'Суббота',
	[Weekdays.sunday]: 'Воскресенье',
};

export const weekdaysRusShort: { [key: number]: string } = {
	0: 'Пн',
	1: 'Вт',
	2: 'Ср',
	3: 'Чт',
	4: 'Пт',
	5: 'Сб',
	6: 'Вс',
};
