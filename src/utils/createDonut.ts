import { IProperties } from '../interfaces/IProperties';
import { fractionIds, fractionsDefault } from '../constants/constants';
import { arc, ascending, pie, select } from 'd3';
import styles from '../components/Map/Map.scss';

function getCount(fractions: number[], id: fractionIds): number {
	return fractions.includes(id) ? 1 : 0;
}

export function createDonut(props: IProperties): HTMLDivElement {
	const div = document.createElement('div');

	const fractions = props.fractions ? props.fractions.split(',').map((id: string) => Number(id)) : [];
	const hasSingleFraction = fractions.length <= 1;
	const data = fractionsDefault.map((fraction) => {
		return {
			type: fraction.fraction_value,
			count: getCount(fractions, fraction.id),
			color: fraction.color,
		};
	});

	const thickness = hasSingleFraction ? 2 : 4;
	const radius = 10;

	const svg = select(div)
		.append('svg')
		.attr('class', styles.pieSvg)
		.attr('width', radius * 3)
		.attr('height', radius * 3);

	const g = svg.append('g').attr('transform', `translate(${radius * 1.5}, ${radius * 1.5})`);

	const circle = g
		.append('circle')
		.attr('r', radius + 2)
		.attr('fill', 'rgba(255,255,255,0.7)')
		.attr('class', 'center-circle');

	const myArc = arc()
		.innerRadius(radius - thickness)
		.outerRadius(radius);

	const myPie = pie()
		// @ts-ignore
		.value((d) => d.count)
		.sort(null);

	const path = g
		.selectAll('path')
		// @ts-ignore
		.data(myPie(data.sort((x, y) => ascending(y.count, x.count))))
		.enter()
		.append('path')
		// @ts-ignore
		.attr('d', myArc)
		// @ts-ignore
		.attr('fill', (d) => d.data.color);

	if (!hasSingleFraction) {
		path.attr('stroke', 'white');
	}

	const text = g
		.append('text')
		.attr('class', styles.pieMarkText)
		.text(props.count)
		.attr('text-anchor', 'middle')
		.attr('dy', 4)
		.attr('fill', '#00000087');

	return div;
}
