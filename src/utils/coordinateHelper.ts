// https://docs.mapbox.com/archive/ios/maps/api/6.1.0/tile-url-templates.html
// https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames

export function lon2tile(lon: number, zoom: number): number {
	return Math.floor(((lon + 180) / 360) * Math.pow(2, zoom));
}

export function lat2tile(lat: number, zoom: number): number {
	return Math.floor(
		((1 - Math.log(Math.tan((lat * Math.PI) / 180) + 1 / Math.cos((lat * Math.PI) / 180)) / Math.PI) / 2) *
			Math.pow(2, zoom),
	);
}

function tile2long(x: number, z: number): number {
	return (x / Math.pow(2, z)) * 360 - 180;
}
function tile2lat(y: number, z: number): number {
	const n = Math.PI - (2 * Math.PI * y) / Math.pow(2, z);

	return (180 / Math.PI) * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n)));
}

// Example for calculating number of tiles within given extent and zoom-level:
// var zoom = 9;
// var top_tile = lat2tile(north_edge, zoom); // eg.lat2tile(34.422, 9);
// var left_tile = lon2tile(west_edge, zoom);
// var bottom_tile = lat2tile(south_edge, zoom);
// var right_tile = lon2tile(east_edge, zoom);
// var width = Math.abs(left_tile - right_tile) + 1;
// var height = Math.abs(top_tile - bottom_tile) + 1;

// total tiles
// var total_tiles = width * height; // -> eg. 377

// Example:
// http://oms.wff.ch/calc.htm
