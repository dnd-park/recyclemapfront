import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
// import * as qs from "qs";
// import { PathLike } from "fs";

declare module 'axios' {
	interface AxiosResponse<T = any> extends Promise<T> {}
}

export const apiConfig = {
	returnRejectedPromiseOnError: true,
	withCredentials: true,
	timeout: 30000,
	// baseURL: '',
	headers: {
		common: {
			'Cache-Control': 'no-cache, no-store, must-revalidate',
			Pragma: 'no-cache',
			'Content-Type': 'application/json',
			Accept: 'application/json',
		},
	},
	// paramsSerializer: (params: PathLike) => qs.stringify(params, { indices: false }),
};

export abstract class HttpClient {
	protected readonly instance: AxiosInstance;

	public constructor(baseURL: string) {
		this.instance = axios.create({
			withCredentials: true,
			baseURL,
		});

		this._initializeResponseInterceptor();
	}

	private _initializeResponseInterceptor = () => {
		this.instance.interceptors.response.use(this._handleResponse, this._handleError);
	};

	private _handleResponse = ({ data }: AxiosResponse) => data;
	// private _handleResponse = (res: AxiosResponse) => res;

	protected _handleError = (error: AxiosError) => {
		console.error(error);

		// https://www.intricatecloud.io/2020/03/how-to-handle-api-errors-in-your-web-app-using-axios/
		switch (true) {
			case Boolean(error.response):
				// client received an error response (5xx, 4xx)
				break;
			case error.request:
				// client never received a response, or request never left
				break;
			default:
				// anything else
				break;
		}

		return Promise.reject<AxiosError>(error);
	};
}
