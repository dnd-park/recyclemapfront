import { IPoint } from '../interfaces/IPoint';
import { Feature, FeatureCollection } from 'geojson';
import { fractionIds, fractionNames } from '../constants/constants';

export default (points: IPoint[]): FeatureCollection => {
	const features: Feature[] = [];

	if (Array.isArray(points)) {
		points.forEach((p) => {
			features.push({
				type: 'Feature',
				// geometry: {
				// 	type: 'Point',
				// 	coordinates: [p.Longitude, p.Latitude],
				// },
				geometry: p.coordinates,
				id: p.id,
				// в properties вставляй только нужные свойства, так как это влияет на скорость рендеринга
				properties: {
					ID: p.id,
					[fractionNames.plastic]: p.fractions.indexOf(fractionIds.plastic) > -1,
					[fractionNames.appliances]: p.fractions.indexOf(fractionIds.appliances) > -1,
					[fractionNames.batteries]: p.fractions.indexOf(fractionIds.batteries) > -1,
					[fractionNames.caps]: p.fractions.indexOf(fractionIds.caps) > -1,
					[fractionNames.clothes]: p.fractions.indexOf(fractionIds.clothes) > -1,
					[fractionNames.glass]: p.fractions.indexOf(fractionIds.glass) > -1,
					[fractionNames.light_bulbs]: p.fractions.indexOf(fractionIds.light_bulbs) > -1,
					[fractionNames.metal]: p.fractions.indexOf(fractionIds.metal) > -1,
					[fractionNames.tetra_pack]: p.fractions.indexOf(fractionIds.tetra_pack) > -1,
					[fractionNames.tires]: p.fractions.indexOf(fractionIds.tires) > -1,
					[fractionNames.toxic]: p.fractions.indexOf(fractionIds.toxic) > -1,
					[fractionNames.paper]: p.fractions.indexOf(fractionIds.paper) > -1,
					[fractionNames.other]: p.fractions.indexOf(fractionIds.other) > -1,
					// Title: p.Title,
				},
			});
		});
	}

	return {
		type: 'FeatureCollection',
		features: features,
	};
};
