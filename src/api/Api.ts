import { HttpClient } from '../utils/httpClient';
import { IPoint } from '../interfaces/IPoint';
import { apiBaseUrl, dadataApiKey, dadataApiSecretKey, mapBoxAccessToken } from '../constants/constants';
import { ILoginData, IUserEditData } from '../interfaces/IUser';
import { IComplaint } from '../interfaces/IComplaint';
import { IGradesRequestData } from '../interfaces/IGrade';
import { IComment } from '../interfaces/IComment';
import { IActionRequestParams, IActionsResponse } from '../interfaces/IAction';

export class MainApi extends HttpClient {
	public constructor() {
		super(apiBaseUrl);
	}

	public getPoints = () => this.instance.get<IPoint[]>('/points');
	public getPointsFiltered = (filters: string) => {
		if (filters) {
			return this.instance.get<IPoint[]>(`/points?filters=${filters}`);
		}

		return this.instance.get<IPoint[]>(`/points`);
	};
	public getPoint = (id: number) => this.instance.get<IPoint>(`/point/${id}`);

	public login = (data: ILoginData) => this.instance.post('/user/login', data);
	public signup = (data: ILoginData) => this.instance.post('/user/signup', data);
	public logout = () => this.instance.post('/user/logout');
	public editUser = (data: IUserEditData) => this.instance.post('/user/edit', data);
	public getCurrentUser = () => this.instance.get('/user');
	public getUser = (id: number) => this.instance.get(`/user/${id}`, { withCredentials: true });

	public insertPoint = (point: IPoint) => this.instance.post('/point/add', point);
	public uploadPointPhotos = (pointId: number, files: FormData) =>
		this.instance.post(`/points/${pointId}/image`, files, {
			headers: {
				'Content-Type': 'multipart/form-data',
			},
		});

	public uploadRequestPhotos = (reqId: number, files: FormData) =>
		this.instance.post(`/user/requests/${reqId}/image`, files, {
			headers: {
				'Content-Type': 'multipart/form-data',
			},
		});

	public insertRequest = (point: IPoint) => this.instance.post('/user/request', point);

	public loadCities = () => this.instance.get('/cities');

	public getMyRequests = () => this.instance.get('/user/requests');
	public getRequests = () => this.instance.get('/admin/requests');

	public getFavorites = () => this.instance.get('/favorite');
	public addToFavorites = (ids: number[]) => this.instance.post('/favorite', { ids });
	public removeFromFavorites = (ids: number[]) => this.instance.post(`/favorite/delete`, { ids });

	public denyRequest = (id: number) => this.instance.put(`/admin/requests/${id}/deny`);
	public approveRequest = (id: number) => this.instance.put(`/admin/requests/${id}/approve`);

	public unpublishPoints = (ids: number[]) =>
		this.instance.put('points/update/status/unpublish', {
			ids,
		});
	public publishPoints = (ids: number[]) =>
		this.instance.put('points/update/status/publish', {
			ids,
		});

	public getFullPoints = () => this.instance.get<IPoint[]>('/admin/points?limit=1000');

	public addComplaint = (pointId: number, data: IComplaint) =>
		this.instance.post(`/points/${pointId}/complaint`, data);

	public getMyComplaints = (email: string) => this.instance.get(`/user/complaints?email=${email}`);

	public setGradeToPoint = (data: IGradesRequestData) => this.instance.put(`/grade`, data);

	public addComment = (pointId: number, data: IComment) => this.instance.post(`/point/${pointId}/comment`, data);

	public getPointComments = (pointId: number) => this.instance.get(`/point/${pointId}/comments`);
	public getMyComments = () => this.instance.get(`/comments`);

	public getActions = (params: IActionRequestParams = {} as any) =>
		this.instance.post<IActionsResponse>(`/actions`, params);

	public geoCode = (query: string) =>
		this.instance.post('https://cleaner.dadata.ru/api/v1/clean/address', [query], {
			headers: {
				'Content-Type': 'application/json',
				Authorization: 'Token ' + dadataApiKey,
				'X-Secret': dadataApiSecretKey,
			},
		});

	public dadataSuggest = (query: string) =>
		fetch('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address', {
			method: 'POST',
			mode: 'cors',
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
				Authorization: 'Token ' + dadataApiKey,
			},
			body: JSON.stringify({ query: query }),
		}).then((response) => response.json());

	public mapboxSuggestions = (query: string) =>
		fetch(
			`https://api.mapbox.com/geocoding/v5/mapbox.places/${query}.json?access_token=${mapBoxAccessToken}&autocomplete=true&country=ru&language=ru`,
			{
				method: 'GET',
				mode: 'cors',
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
				},
			},
		).then((response) => response.json());

	public mapboxReverseGeocode = (lng: number, ltd: number) =>
		fetch(
			`https://api.mapbox.com/geocoding/v5/mapbox.places/${lng},${ltd}.json?access_token=${mapBoxAccessToken}&autocomplete=true&country=ru&language=ru`,
			{
				method: 'GET',
				mode: 'cors',
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
				},
			},
		).then((response) => response.json());
}

// class MainApiProtected extends HttpClient {
// 	public constructor() {
// 		super(apiBaseUrl);
//
// 		this._initializeRequestInterceptor();
// 	}
//
// 	private _initializeRequestInterceptor = () => {
// 		this.instance.interceptors.request.use(
// 			this._handleRequest,
// 			this._handleError,
// 		);
// 	};
//
// 	private _handleRequest = (config: AxiosRequestConfig) => {
// 		config.headers['Authorization'] = 'Bearer ...';
//
// 		return config;
// 	};
//
// 	public createUser = (body: any) => this.instance.post('/users', body);
// }
