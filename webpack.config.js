const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const dotenv = require('dotenv');
const AntdScssThemePlugin = require('antd-scss-theme-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const lessToJs = require('less-vars-to-js');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const env = dotenv.config({ path: '.env.local' }).parsed || {};

const envKeys = Object.keys(env).reduce((prev, next) => {
	prev[`process.env.${next}`] = JSON.stringify(env[next]);
	return prev;
}, {});

// https://medium.com/@hydrock/%D0%BA%D0%B0%D0%BA-%D0%BA%D0%B0%D1%81%D1%82%D0%BE%D0%BC%D0%B8%D0%B7%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C-ant-design-%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D1%83%D1%8F-react-webpack-%D0%BF%D0%BE%D1%82%D0%B5%D1%80%D1%8F%D0%BD%D0%BD%D0%B0%D1%8F-%D0%B8%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F-b3d90958eaec
const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, './src/styles/theme.less'), 'utf8'));
// lessToJs does not support @icon-url: "some-string", so we are manually adding it to the produced themeVariables js object here
// themeVariables["@icon-url"] = "'//localhost:8080/fonts/iconfont'";

module.exports = {
	entry: {
		main: path.resolve(__dirname, 'src/index.tsx'),
	},
	output: {
		path: path.resolve(__dirname, 'public'),
		publicPath: '/',
		filename: 'bundle.js',
	},
	mode: process.env.NODE_ENV || 'development',
	performance: {
		hints: 'warning',
		maxEntrypointSize: 300 * 1000, // цифру можно менять в зависимости от размера приложения
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: [
					{
						loader: 'babel-loader',
					},
					{
						loader: 'ts-loader',
						options: {
							allowTsInNodeModules: true,
							// configFile: process.env.DEPLOY
							// 	? 'tsconfig.build.json'
							// 	: 'tsconfig.json',
							configFile: 'tsconfig.json',
						},
					},
				],
			},
			{
				test: /\.(jpg|svg)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[path][name].[ext]',
							context: path.resolve(__dirname, 'src/'),
							outputPath: './',
							// publicPath: '../dist',
							publicPath: '/',
							useRelativePaths: true,
							esModule: false,
						},
					},
				],
			},
			{
				test: /\.(png|jpeg|gif|woff|woff2|eot|ttf)$/,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 200 * 1024,
						},
					},
				],
				exclude: /(node_modules)/,
			},
			{
				test: /\.css$/,
				use: [
					process.env.NODE_ENV === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
					'css-loader',
				],
			},
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					process.env.NODE_ENV === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
					{
						loader: 'css-loader',
						options: {
							importLoaders: 2,
							modules: {
								localIdentName: '[name]__[local]--[hash:base64:5]',
								// hashPrefix: name,
								exportLocalsConvention: 'camelCase',
							},
						},
					},
					'postcss-loader',
					// AntdScssThemePlugin.themify('sass-loader'),
					'sass-loader',
				],
			},
			{
				test: /\.less$/,
				use: [
					process.env.NODE_ENV === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
					{
						loader: 'css-loader',
						options: {
							importLoaders: 1,
							// sourceMap: process.env.NODE_ENV !== 'production',
						},
					},
					// AntdScssThemePlugin.themify('less-loader'),
					{
						loader: 'less-loader',
						options: {
							lessOptions: {
								// modifyVars: {
								// 	'primary-color': 'green',
								// },
								modifyVars: themeVariables,
								javascriptEnabled: true,
							},
							// javascriptEnabled: true,
						},
					},
				],
			},
		],
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js'],
		alias: {
			'@': path.join(__dirname, 'src'),
		},
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env.MAPBOX_TOKEN': JSON.stringify(process.env.MAPBOX_TOKEN),
			'process.env.DADATA_API_KEY': JSON.stringify(process.env.DADATA_API_KEY),
			...envKeys,
		}),
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			title: 'Caching',
			template: './src/index.html',
		}),
		// new HtmlWebpackPlugin({
		// 	inject: true,
		// 	template: './src/index.html',
		// }),
		// theme antd https://github.com/intoli/antd-scss-theme-plugin
		// new AntdScssThemePlugin(path.join(__dirname, 'src', 'styles', 'theme.scss')),
		new MiniCssExtractPlugin({
			filename: '[name].[hash].css',
		}),
		new CopyPlugin({
			patterns: [
				{
					from: path.resolve(__dirname, 'src/_redirects'),
				},
			],
		}),
	],
	optimization: {
		runtimeChunk: 'single',
		minimizer: [
			// new TerserPlugin({}),
			new OptimizeCSSAssetsPlugin({}),
			new CssMinimizerPlugin(),
		],
		minimize: true,
	},
	devtool: 'inline-source-map',
	// devtool: 'source-map',
	devServer: {
		contentBase: path.join(__dirname, 'public/'),
		compress: true,
		inline: true,
		port: 8080,
		// publicPath: 'http://localhost:8080/dist/',
		publicPath: 'http://localhost:8080/',
		// hotOnly: true,
		headers: {
			'Access-Control-Allow-Origin': '*',
		},
		historyApiFallback: true,
	},
};
